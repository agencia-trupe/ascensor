<?php

$lang['profiler_database'] = 'BASE DE DADOS';
$lang['profiler_controller_info'] = 'CLASSE/M�TODO';
$lang['profiler_benchmarks'] = 'BENCHMARKS';
$lang['profiler_queries'] = 'COLSULTAS';
$lang['profiler_get_data'] = 'DADOS GET';
$lang['profiler_post_data'] = 'DADOS POST';
$lang['profiler_uri_string'] = 'URI STRING';
$lang['profiler_memory_usage'] = 'USO DA MEM�RIA';
$lang['profiler_config'] = 'VARI�VEIS DE CONFIGURA��O';
$lang['profiler_headers'] = 'CABE�ALHOS HTTP';
$lang['profiler_no_db'] = 'O driver da base de dados n�o foi carregado';
$lang['profiler_no_queries'] = 'Nenhuma consulta foi executada';
$lang['profiler_no_post'] = 'N�o existem dados do tipo POST';
$lang['profiler_no_get'] = 'N�o existem dados do tipo GET';
$lang['profiler_no_uri'] = 'N�o existem dados do tipo URI';
$lang['profiler_no_memory'] = 'Uso da mem�ria n�o est� dispon�vel';
$lang['profiler_no_profiles'] = 'N�o h� dados do perfil - todas as se��es de perfis foram desativados.';
?>