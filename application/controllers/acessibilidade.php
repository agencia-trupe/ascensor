<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Acessibilidade extends CI_controller {

	var $header;

    function __construct() {
        parent::__construct();

        $this->header['topo'] = array(
			array(
				'imagem' => 'acessbilidade-banner.jpg',
				'titulo' => 'Autonomia,<br>segurança e<br>conforto',
				'texto'  => ''
			)
		);
    }

    function index(){
    	redirect('index.php/acessibilidade/dois');
    }

    function dois() {
        $this->load->view('common/header', $this->header);
        $this->load->view('acessibilidade/submenu');
        $this->load->view('acessibilidade/dois');
        $this->load->view('common/footer');
    }

    function quatro() {
        $this->load->view('common/header', $this->header);
        $this->load->view('acessibilidade/submenu');
        $this->load->view('acessibilidade/quatro');
        $this->load->view('common/footer');
    }    

}
?>
