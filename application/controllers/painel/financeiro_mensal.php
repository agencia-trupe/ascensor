<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Financeiro_mensal extends CI_controller {

    function __construct() {
        parent::__construct();

        if(!$this->session->userdata('logged_in'))
            redirect('/index.php/painel/');
    }


    function index($cliente = FALSE, $pag = 0) {
        $conf = array(
            'per_page' => '12',
            'first_link' => FALSE,
            'last_link' => FALSE,
            'next_link' => FALSE,
            'prev_link' => FALSE,
            'next_tag_open' => "<span class='prox'>",
            'next_tag_close' => '</span>',
            'prev_tag_open' => "<span class='ant'>",
            'prev_tag_close' => '</span>',
            'display_pages' => TRUE,
            'num_tag_open' => "<span class='curved'>",
            'num_tag_close' => "</span>",
            'cur_tag_open' => "<span class='curved'>",
            'cur_tag_close' => "</span>",
            'base_url' => base_url().'painel/financeiro_mensal/index/'.$cliente.'/',
            'uri_segment' => 5
        );

        $data['cliente'] = $cliente;

        if($data['cliente']){
            $this->db->where('cliente', $cliente);
            $conf['total_rows'] = $this->db->count_all_results('valor_contratacao');

            $this->load->library('pagination', $conf);
            $data['paginacao'] = $this->pagination->create_links();

            $data['registros'] = $this->db
                    ->order_by('data_vencimento', 'DESC')
                    ->where('cliente', $cliente)
                    ->get('valor_contratacao', $conf['per_page'], $pag)
                    ->result();

            $data['clientes'] = $this->db->order_by('nome')->get('clientes')->result();

            foreach($data['registros'] as $reg => $val){
                $val->valor = valor($val->valor, 'exibir');
                $val->cliente = $this->pegaNomeCliente($val->cliente);
                $val->mes = pegaMes($val->data_vencimento, 'mysql');
            }

        }else{
            $data['clientes'] = $this->db->order_by('nome')->get('clientes')->result();
        }

        $this->load->view('painel/common/header');
        $this->load->view('painel/common/menu');
        $this->load->view('painel/financeiro/mensal', $data);
        $this->load->view('painel/common/footer');
    }

    function form($id = FALSE){
        if($id)
            $data['registro'] = $this->db->get_where('valor_contratacao', array('id' => $id))->result();
        else
            $data['registro'] = FALSE;

        $data['clientes'] = $this->db->order_by('nome', 'ASC')->get('clientes')->result();
        
        $this->load->view('painel/common/header');
        $this->load->view('painel/common/menu');
        $this->load->view('painel/financeiro/mensal_form', $data);
        $this->load->view('painel/common/footer');
    }

    function inserir(){
        $this->db->set('cliente', $this->input->post('cliente'));
        $this->db->set('data_vencimento', formataData($this->input->post('data_vencimento'), 'br2mysql'));
        $this->db->set('valor', valor($this->input->post('valor'), 'banco'));
        $this->db->set('descricao', $this->input->post('descricao'));
        
        $this->db->insert('valor_contratacao');
        redirect('index.php/painel/financeiro_mensal/index/'.$this->input->post('cliente'));
    }

    function editar($id){
        $this->db->set('cliente', $this->input->post('cliente'));
        $this->db->set('data_vencimento', formataData($this->input->post('data_vencimento'), 'br2mysql'));
        $this->db->set('valor', valor($this->input->post('valor'), 'banco'));
        $this->db->set('descricao', $this->input->post('descricao'));
        $pago = $this->input->post('pago');
        if($pago == 1)
            $this->db->set('data_pagamento', formataData($this->input->post('data_pagamento') ,'br2mysql'));

        $this->db->where('id', $id);
        $this->db->update('valor_contratacao');
        redirect('index.php/painel/financeiro_mensal/index/'.$this->input->post('cliente'));
    }

    function excluir($id){
        $cliente = $this->pegaIdCliente($id);
        $this->db->delete('valor_contratacao', array('id' => $id));
        redirect('index.php/painel/financeiro_mensal/index/'.$cliente);
    }

    private function pegaNomeCliente($id_cliente){
        $query = $this->db->get_where('clientes', array('id' => $id_cliente))->result();
        return $query[0]->nome;
    }

    private function pegaIdCliente($id_registro){
        $query = $this->db->get_where('valor_contratacao', array('id' => $id_registro))->result();
        return $query[0]->cliente;
    }

    function geraTabela(){
        
        $pag = 0;
        $conf = array(
            'per_page' => '12',
            'first_link' => FALSE,
            'last_link' => FALSE,
            'next_link' => FALSE,
            'prev_link' => FALSE,
            'next_tag_open' => "<span class='prox'>",
            'next_tag_close' => '</span>',
            'prev_tag_open' => "<span class='ant'>",
            'prev_tag_close' => '</span>',
            'display_pages' => TRUE,
            'num_tag_open' => "<span class='curved'>",
            'num_tag_close' => "</span>",
            'cur_tag_open' => "<span class='curved'>",
            'cur_tag_close' => "</span>",
            'base_url' => base_url().'painel/financeiro_mensal/index/'.$this->input->post('cliente').'/',
            'uri_segment' => 5
        );


        $this->db->where('cliente', $this->input->post('cliente'));

        $conf['total_rows'] = $this->db->count_all_results('valor_contratacao');
        $data['registros'] = $this->db
                                ->order_by('data_vencimento', 'DESC')
                                ->where('cliente', $this->input->post('cliente'))
                                ->get('valor_contratacao', $conf['per_page'], $pag)
                                ->result();

        foreach($data['registros'] as $reg => $val){
            $val->cliente = $this->pegaNomeCliente($val->cliente);
            $val->valor = valor($val->valor, 'exibir');
            $val->data_vencimento = formataData($val->data_vencimento, 'mysql2br');
            $val->mes = pegaMes($val->data_vencimento, 'br');
            if($val->data_pagamento)
                $val->data_pagamento = formataData($val->data_pagamento, 'mysql2br');
        }

        $this->load->library('pagination', $conf);
        $paginacao = $this->pagination->create_links();

        $retorno = "<br/><br/>";

        $retorno .= "<table>";

            $retorno .= "<thead>";
            $retorno .= "<tr>";
                $retorno .= "<th>Cliente</th>";
                $retorno .= "<th>Valor (R$)</th>";
                $retorno .= "<th>Mês</th>";
                $retorno .= "<th>Data de Vencimento</th>";
                $retorno .= "<th>Pago</th>";
                $retorno .= "<th></th>";
                $retorno .= "<th></th>";
            $retorno .= "</tr>";
            $retorno .= "</thead>";

            if($paginacao):
                $retorno .= "<tfoot>";
                    $retorno .= "<tr>";
                        $retorno .= "<td colspan='5' class='pagination'>";
                         $retorno .= $paginacao;
                        $retorno .= "</td>";
                    $retorno .= "</tr>";
                $retorno .= "</tfoot>";
            endif;

            foreach($data['registros'] as $reg => $val):

                $retorno .= "<tbody>";
                $retorno .= "<tr>";
                    $retorno .= "<td>".$val->cliente."</td>";
                    $retorno .= "<td>".$val->valor."</td>";
                    $retorno .= "<td>".$val->mes."</td>";
                    $retorno .= "<td>".$val->data_vencimento."</td>";
                    $retorno .= ($val->data_pagamento) ? "<td>PAGO(".$val->data_pagamento.")</td>" : "<td></td>";
                    $retorno .= "<td><a href='".base_url()."painel/financeiro_mensal/form/".$val->id."' class=\"edit\">editar</a></td>";
                    $retorno .= "<td><a href='".base_url()."painel/financeiro_mensal/excluir/".$val->id."' class=\"delete\">excluir</a></td>";
                $retorno .= "</tr>";
                $retorno .= "</tbody>";

            endforeach;

        $retorno .= "</table>";

        echo $retorno;
        //echo $this->db->last_query();
    }
}
?>
