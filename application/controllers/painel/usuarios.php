<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Usuarios extends CI_controller {

    var $titulo = 'Usuários';

    function  __construct() {
        parent::__construct();

        if(!$this->session->userdata('logged_in'))
            redirect('index.php//painel/');

        $this->load->model('painel/usuarios_model');
    }

    function index() {
        $data['titulo'] = $this->titulo;
        $data['registros'] = $this->usuarios_model->dadosUsuarios();

        $this->load->view('painel/common/header');
        $this->load->view('painel/common/menu');
        $this->load->view('painel/usuarios', $data);
        $this->load->view('painel/common/footer');
    }

    function form($id = null){
        $data['titulo'] = $this->titulo;
        if($id)
            $data['registro'] = $this->usuarios_model->dadosUsuarios($id);

        $this->load->view('painel/common/header');
        $this->load->view('painel/common/menu');
        $this->load->view('painel/usuarios_form', $data);
        $this->load->view('painel/common/footer');
    }

    function inserir(){
        $this->usuarios_model->inserir();
        redirect('index.php/painel/usuarios/');
    }

    function editar($id){
        $this->usuarios_model->editar($id);
        redirect('index.php/painel/usuarios/');
    }

    function excluir($id){
        $this->usuarios_model->excluir($id);
        redirect('index.php/painel/usuarios/');
    }

}
?>
