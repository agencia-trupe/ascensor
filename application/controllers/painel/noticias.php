<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Noticias extends CI_controller {


    function __construct() {
        parent::__construct();

        if(!$this->session->userdata('logged_in'))
            redirect('index.php//painel/');
    }


    function index() {
        $data['registros'] = $this->db->order_by('titulo', 'DESC')->get('informacoes')->result();

        $this->load->view('painel/common/header');
        $this->load->view('painel/common/menu');
        $this->load->view('painel/noticias', $data);
        $this->load->view('painel/common/footer');
    }

    function form($id = FALSE){
        if($id)
            $data['registro'] = $this->db->get_where('informacoes', array('id' => $id))->result();
        else
            $data['registro'] = FALSE;

        $this->load->view('painel/common/header');
        $this->load->view('painel/common/menu');
        $this->load->view('painel/noticias_form', $data);
        $this->load->view('painel/common/footer');
    }

    function inserir(){
        $this->db->set('titulo', $this->input->post('titulo'))
                 ->set('slug', url_title($this->input->post('titulo'), '_', TRUE))
                 ->set('texto', $this->input->post('texto'))
                 ->set('olho', $this->input->post('olho'))
                 ->insert('informacoes');
                 
        redirect('index.php/painel/noticias');
    }

    function editar($id){
        $this->db->set('titulo', $this->input->post('titulo'))
                 ->set('slug', url_title($this->input->post('titulo'), '_', TRUE))
                 ->set('texto', $this->input->post('texto'))
                 ->set('olho', $this->input->post('olho'))
                 ->where('id', $id)
                 ->update('informacoes');

        redirect('index.php/painel/noticias');
    }


    function excluir($id){
        $this->db->delete('informacoes', array('id' => $id));
        redirect('index.php/painel/noticias');
    }
}
?>
