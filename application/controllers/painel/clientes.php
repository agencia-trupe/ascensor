<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Clientes extends CI_controller {

    function __construct() {
        parent::__construct();

        if(!$this->session->userdata('logged_in'))
            redirect('index.php//painel/');
    }


    function index() {
        $data['registros'] = $this->db->get('clientes')->result();

        $this->load->view('painel/common/header');
        $this->load->view('painel/common/menu');
        $this->load->view('painel/clientes', $data);
        $this->load->view('painel/common/footer');
    }

    function form($id = FALSE){
        if($id)
            $data['registro'] = $this->db->get_where('clientes', array('id' => $id))->result();
        else
            $data['registro'] = FALSE;

        $this->load->view('painel/common/header');
        $this->load->view('painel/common/menu');
        $this->load->view('painel/clientes_form', $data);
        $this->load->view('painel/common/footer');
    }

    function inserir(){
        $this->db->set('nome', $this->input->post('nome'));
        $this->db->set('endereco', $this->input->post('endereco'));
        $this->db->set('telefone1', $this->input->post('telefone1'));
        $this->db->set('telefone2', $this->input->post('telefone2'));
        $this->db->set('username', $this->input->post('username'));
        $this->db->set('password', md5($this->input->post('password')));
        $this->db->insert('clientes');
        redirect('index.php/painel/clientes/');
    }

    function editar($id){
        $this->db->set('nome', $this->input->post('nome'));
        $this->db->set('endereco', $this->input->post('endereco'));
        $this->db->set('telefone1', $this->input->post('telefone1'));
        $this->db->set('telefone2', $this->input->post('telefone2'));
        $this->db->set('username', $this->input->post('username'));

        if($this->input->post('password') != ''){
            $this->db->set('password', md5($this->input->post('password')));
        }
        $this->db->where('id', $id);
        $this->db->update('clientes');
        redirect('index.php/painel/clientes/');
    }

    function excluir($id){
        $this->db->delete('clientes', array('id' => $id));
        redirect('index.php/painel/clientes/');
    }

}
?>
