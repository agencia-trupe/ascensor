<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Visitas extends CI_controller {

    function __construct() {
        parent::__construct();

        if(!$this->session->userdata('logged_in'))
            redirect('index.php//painel/');
    }


    function index($cliente = FALSE, $pag = 0) {
        $conf = array(
            'per_page' => '15',
            'first_link' => FALSE,
            'last_link' => FALSE,
            'next_link' => FALSE,
            'prev_link' => FALSE,
            'next_tag_open' => "<span class='prox'>",
            'next_tag_close' => '</span>',
            'prev_tag_open' => "<span class='ant'>",
            'prev_tag_close' => '</span>',
            'display_pages' => TRUE,
            'num_tag_open' => "<span class='curved'>",
            'num_tag_close' => "</span>",
            'cur_tag_open' => "<span class='curved'>",
            'cur_tag_close' => "</span>",
            'base_url' => base_url().'painel/visitas/index/'.$cliente.'/',
            'uri_segment' => 5
        );

        $this->db->where('cliente', $cliente);

        $conf['total_rows'] = $this->db->count_all_results('visitas');
        
        $this->load->library('pagination', $conf);
        $data['paginacao'] = $this->pagination->create_links();
        
        if($cliente){
            $data['registros'] = $this->db
                                    ->order_by('data', 'DESC')
                                    ->where('cliente', $cliente)->get('visitas', $conf['per_page'], $pag)->result();

            $data['clientes'] = $this->db->order_by('nome')->get('clientes')->result();
            $data['cliente'] = $cliente;
            foreach($data['registros'] as $reg => $val){
                $val->tecnico = $this->getNome('tecnicos', $val->tecnico);
                $val->cliente = $this->getNome('clientes', $val->cliente);
                $val->data    = $this->separaDataHora($val->data);
            }
        }else{
            $data['registros'] = FALSE;
            $data['clientes'] = $this->db->order_by('nome')->get('clientes')->result();
        }

        $this->load->view('painel/common/header');
        $this->load->view('painel/common/menu');
        $this->load->view('painel/visitas', $data);
        $this->load->view('painel/common/footer');
    }

    function form($id = FALSE){
        if($id){
            $data['registro'] = $this->db->get_where('visitas', array('id' => $id))->result();
            $data['registro'][0]->data = $this->separaDataHora($data['registro'][0]->data);
        }else
            $data['registro'] = FALSE;
        
        $data['clientes'] = $this->db->order_by('nome', 'ASC')->get('clientes')->result();
        $data['tecnicos'] = $this->db->order_by('nome', 'ASC')->get('tecnicos')->result();

        $this->load->view('painel/common/header');
        $this->load->view('painel/common/menu');
        $this->load->view('painel/visitas_form', $data);
        $this->load->view('painel/common/footer');
    }

    function inserir(){
        $data = $this->input->post('data');
        $hora = $this->input->post('horario');
        $this->db->set('data', $this->juntaDataHora($data, $hora));
        $this->db->set('ocorrencia', $this->input->post('ocorrencia'));
        $this->db->set('tecnico', $this->input->post('tecnico'));
        $this->db->set('cliente', $this->input->post('cliente'));

        $this->db->insert('visitas');
        redirect('index.php/painel/visitas/index/'.$this->input->post('cliente'));
    }

    function editar($id){
        $data = $this->input->post('data');
        $hora = $this->input->post('horario');
        $this->db->set('data', $this->juntaDataHora($data, $hora));
        $this->db->set('ocorrencia', $this->input->post('ocorrencia'));
        $this->db->set('tecnico', $this->input->post('tecnico'));
        $this->db->set('cliente', $this->input->post('cliente'));
        $this->db->where('id', $id);
        $this->db->update('visitas');
        redirect('index.php/painel/visitas/index/'.$this->input->post('cliente'));
    }

    function excluir($id){
        $cliente = $this->pegaCliente($id);
        $this->db->delete('visitas', array('id' => $id));
        redirect('index.php/painel/visitas/index/'.$cliente);
    }

    private function getNome($tabela, $id){
        $query = $this->db->get_where($tabela, array('id' => $id))->result();
        return $query[0]->nome;
    }

    private function pegaCliente($id){
        $query = $this->db->get_where('visitas', array('id' => $id))->result();
        return $query[0]->cliente;
    }

    private function separaDataHora($datetime){
        list($data, $hora) = explode(' ', $datetime);
        return array('data' => formataData($data, 'mysql2br'), 'hora' => formataHora($hora, 'mysql2br'));
    }

    private function juntaDataHora($data, $hora){
        return formataData($data, 'br2mysql').' '.formataHora($hora, 'br2mysql');
    }

    function geraTabela(){
        $pag = 0;
        $conf = array(
            'per_page' => '15',
            'first_link' => FALSE,
            'last_link' => FALSE,
            'next_link' => FALSE,
            'prev_link' => FALSE,
            'next_tag_open' => "<span class='prox'>",
            'next_tag_close' => '</span>',
            'prev_tag_open' => "<span class='ant'>",
            'prev_tag_close' => '</span>',
            'display_pages' => TRUE,
            'num_tag_open' => "<span class='curved'>",
            'num_tag_close' => "</span>",
            'cur_tag_open' => "<span class='curved'>",
            'cur_tag_close' => "</span>",
            'base_url' => base_url().'painel/visitas/index/'.$this->input->post('cliente').'/',
            'uri_segment' => 5
        );
        
        
        $this->db->where('cliente', $this->input->post('cliente'));

        $conf['total_rows'] = $this->db->count_all_results('visitas');
        $data['registros'] = $this->db
                                    ->order_by('data', 'DESC')
                                    ->where('cliente', $this->input->post('cliente'))->get('visitas', $conf['per_page'], $pag)->result();

        $this->load->library('pagination', $conf);
        $paginacao = $this->pagination->create_links();

        foreach($data['registros'] as $reg => $val){
            $val->tecnico = $this->getNome('tecnicos', $val->tecnico);
            $val->cliente = $this->getNome('clientes', $val->cliente);
            $val->data    = $this->separaDataHora($val->data);
        }    
        
        $retorno = "";

        // Adicionar select de clientes

        $clientes = $this->db->order_by('nome')->get('clientes')->result();
        $retorno .= "<h3>Selecione o cliente</h3>";
            $retorno .= "<select name='cliente' id='select-cliente-visita'>";
                $retorno .= "<option value=''></option>";
                foreach($clientes as $cli):
                    $retorno .= "<option value='".$cli->id;
                    $retorno .= ($cli->id == $this->input->post('cliente')) ? "' selected>".$cli->nome."</option>" : "'>".$cli->nome."</option>";
                endforeach;
            $retorno .= "</select><br/><br/>";

        $retorno .= "<table>";
        
            $retorno .= "<thead>";
            $retorno .= "<tr>";
                $retorno .= "<th>Cliente</th>";
                $retorno .= "<th>Data</th>";
                $retorno .= "<th>Descrição</th>";
                $retorno .= "<th></th>";
                $retorno .= "<th></th>";
            $retorno .= "</tr>";
            $retorno .= "</thead>";
            
            if($paginacao):
                $retorno .= "<tfoot>";
                    $retorno .= "<tr>";
                        $retorno .= "<td colspan='5' class='pagination'>";
                         $retorno .= $paginacao;
                        $retorno .= "</td>";
                    $retorno .= "</tr>";
                $retorno .= "</tfoot>";
            endif;
            
            foreach($data['registros'] as $reg => $val):

                $retorno .= "<tbody>";
                $retorno .= "<tr>";
                    $retorno .= "<td>".$val->cliente."</td>";
                    $retorno .= "<td>".$val->data['data']."</td>";
                    $retorno .= (strlen($val->ocorrencia) > 30) ? "<td>". substr($val->ocorrencia, 0, 30).'...</td>' : '<td>'.$val->ocorrencia."</td>";
                    $retorno .= "<td><a href='".base_url()."painel/visitas/form/".$val->id."' class=\"edit\">editar</a></td>";
                    $retorno .= "<td><a href='".base_url()."painel/visitas/excluir/".$val->id."' class=\"delete\">excluir</a></td>";
                $retorno .= "</tr>";
                $retorno .= "</tbody>";

            endforeach;

        $retorno .= "</table>";
        
        echo $retorno;
        //echo $this->db->last_query();
    }

}
?>
