<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Financeiro extends CI_controller {

    function __construct() {
        parent::__construct();

        if(!$this->session->userdata('logged_in'))
            redirect('index.php/painel/');
    }


    function index($cliente = FALSE, $mes = FALSE) {
        $data['clientes'] = $this->db->order_by('nome')->get('clientes')->result();

        $data['cliente'] = $cliente;
        $data['mes'] = $mes;

        $this->load->view('painel/common/header');
        $this->load->view('painel/common/menu');
        $this->load->view('painel/financeiro/total', $data);
        $this->load->view('painel/common/footer');
    }

    function geraTabela(){

        $cliente = $this->input->post('cliente');
        $mes = $this->input->post('mes');

        $total['contratacao'] = $this->pegaTotal($cliente, $mes, 'valor_contratacao');
        $total['pecas'] = $this->pegaTotal($cliente, $mes, 'valor_pecas');
        $total['servicos'] = $this->pegaTotal($cliente, $mes, 'valor_servicos');
        $total['soma'] = $total['contratacao'] + $total['pecas'] + $total['servicos'];

        $retorno = "<br><table>";
        
            $retorno .= "<tr>";
                $retorno .= "<th>Contratação</th>";
                $retorno .= "<th>Peças</th>";
                $retorno .= "<th>Serviço</th>";
                $retorno .= "<th>Total</th>";
            $retorno .= "</tr>";

            $retorno .= "<tr>";
                $retorno .= "<td>".valor($total['contratacao'],'exibir')."</td>";
                $retorno .= "<td>".valor($total['pecas'],'exibir')."</td>";
                $retorno .= "<td>".valor($total['servicos'],'exibir')."</td>";
                $retorno .= "<td>".valor($total['soma'],'exibir')."</td>";
            $retorno .= "</tr>";
            

        $retorno .= "</table>";

        echo $retorno;

    }

    private function pegaTotal($cliente, $mes, $tabela){
        $campo_data = ($tabela == 'valor_contratacao') ? 'data_vencimento' : 'data';
        $query = $this->db->where('cliente', $cliente)
                          ->where("MONTH($campo_data)", $mes)
                          ->where("YEAR($campo_data)", Date('Y'))
                          ->get($tabela)
                          ->result();
        $retorno = 0;
        foreach($query as $q){
            $retorno += $q->valor;
        }
        return $retorno;
    }

}
?>
