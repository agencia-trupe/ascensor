<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Financeiro_pecas extends CI_controller {

    function __construct() {
        parent::__construct();

        if(!$this->session->userdata('logged_in'))
            redirect('index.php//painel/');
    }

    function index($cliente = FALSE, $pag = 0) {
        $conf = array(
            'per_page' => '12',
            'first_link' => FALSE,
            'last_link' => FALSE,
            'next_link' => FALSE,
            'prev_link' => FALSE,
            'next_tag_open' => "<span class='prox'>",
            'next_tag_close' => '</span>',
            'prev_tag_open' => "<span class='ant'>",
            'prev_tag_close' => '</span>',
            'display_pages' => TRUE,
            'num_tag_open' => "<span class='curved'>",
            'num_tag_close' => "</span>",
            'cur_tag_open' => "<span class='curved'>",
            'cur_tag_close' => "</span>",
            'base_url' => base_url().'painel/financeiro_pecas/index/'.$cliente.'/',
            'uri_segment' => 5
        );

        $data['cliente'] = $cliente;

        if($data['cliente']){
            $this->db->where('cliente', $cliente);
            $conf['total_rows'] = $this->db->count_all_results('valor_pecas');

            $this->load->library('pagination', $conf);
            $data['paginacao'] = $this->pagination->create_links();

            $data['registros'] = $this->db
                    ->order_by('data', 'DESC')
                    ->where('cliente', $cliente)
                    ->get('valor_pecas', $conf['per_page'], $pag)
                    ->result();

            $data['clientes'] = $this->db->order_by('nome')->get('clientes')->result();

            foreach($data['registros'] as $reg => $val){
                $val->valor = valor($val->valor, 'exibir');
                $val->cliente = $this->pegaNomeCliente($val->cliente);
            }

        }else{
            $data['clientes'] = $this->db->order_by('nome')->get('clientes')->result();
        }

        $this->load->view('painel/common/header');
        $this->load->view('painel/common/menu');
        $this->load->view('painel/financeiro/pecas', $data);
        $this->load->view('painel/common/footer');
    }

    function form($id = FALSE){
        if($id)
            $data['registro'] = $this->db->get_where('valor_pecas', array('id' => $id))->result();
        else
            $data['registro'] = FALSE;

        $data['clientes'] = $this->db->order_by('nome', 'ASC')->get('clientes')->result();

        $this->load->view('painel/common/header');
        $this->load->view('painel/common/menu');
        $this->load->view('painel/financeiro/pecas_form', $data);
        $this->load->view('painel/common/footer');
    }

    function inserir(){
        $this->db->set('cliente', $this->input->post('cliente'));
        $this->db->set('data', formataData($this->input->post('data'), 'br2mysql'));
        $this->db->set('valor', valor($this->input->post('valor'), 'banco'));
        $this->db->set('descritivo', $this->input->post('descricao'));
        $this->db->set('quantidade', $this->input->post('quantidade'));
        $this->db->set('produto', $this->input->post('produto'));

        $this->db->insert('valor_pecas');
        redirect('index.php/painel/financeiro_pecas/index/'.$this->input->post('cliente'));
    }

    function editar($id){
        $this->db->set('cliente', $this->input->post('cliente'));
        $this->db->set('data', formataData($this->input->post('data'), 'br2mysql'));
        $this->db->set('valor', valor($this->input->post('valor'), 'banco'));
        $this->db->set('descritivo', $this->input->post('descricao'));
        $this->db->set('quantidade', $this->input->post('quantidade'));
        $this->db->set('produto', $this->input->post('produto'));

        $this->db->where('id', $id);
        $this->db->update('valor_pecas');
        redirect('index.php/painel/financeiro_pecas/index/'.$this->input->post('cliente'));
    }

    function excluir($id){
        $cliente = $this->pegaIdCliente($id);
        $this->db->delete('valor_pecas', array('id' => $id));
        redirect('index.php/painel/financeiro_pecas/index/'.$cliente);
    }

    private function pegaNomeCliente($id_cliente){
        $query = $this->db->get_where('clientes', array('id' => $id_cliente))->result();
        return $query[0]->nome;
    }

    private function pegaIdCliente($id_registro){
        $query = $this->db->get_where('valor_pecas', array('id' => $id_registro))->result();
        return $query[0]->cliente;
    }

    function geraTabela(){

        $pag = 0;
        $conf = array(
            'per_page' => '12',
            'first_link' => FALSE,
            'last_link' => FALSE,
            'next_link' => FALSE,
            'prev_link' => FALSE,
            'next_tag_open' => "<span class='prox'>",
            'next_tag_close' => '</span>',
            'prev_tag_open' => "<span class='ant'>",
            'prev_tag_close' => '</span>',
            'display_pages' => TRUE,
            'num_tag_open' => "<span class='curved'>",
            'num_tag_close' => "</span>",
            'cur_tag_open' => "<span class='curved'>",
            'cur_tag_close' => "</span>",
            'base_url' => base_url().'painel/financeiro_pecas/index/'.$this->input->post('cliente').'/',
            'uri_segment' => 5
        );


        $this->db->where('cliente', $this->input->post('cliente'));

        $conf['total_rows'] = $this->db->count_all_results('valor_pecas');
        $data['registros'] = $this->db
                                ->order_by('data', 'DESC')
                                ->where('cliente', $this->input->post('cliente'))
                                ->get('valor_pecas', $conf['per_page'], $pag)
                                ->result();

        foreach($data['registros'] as $reg => $val){
            $val->cliente = $this->pegaNomeCliente($val->cliente);
            $val->valor = valor($val->valor, 'exibir');
            $val->data = formataData($val->data, 'mysql2br');
        }

        $this->load->library('pagination', $conf);
        $paginacao = $this->pagination->create_links();

        $retorno = "<br/><br/>";

        $retorno .= "<table>";

            $retorno .= "<thead>";
            $retorno .= "<tr>";
                $retorno .= "<th>Cliente</th>";
                $retorno .= "<th>Data</th>";
                $retorno .= "<th>Produto</th>";
                $retorno .= "<th>Quantidade</th>";
                $retorno .= "<th>Valor (R$)</th>";
                $retorno .= "<th></th>";
                $retorno .= "<th></th>";
            $retorno .= "</tr>";
            $retorno .= "</thead>";

            if($paginacao):
                $retorno .= "<tfoot>";
                    $retorno .= "<tr>";
                        $retorno .= "<td colspan='7' class='pagination'>";
                         $retorno .= $paginacao;
                        $retorno .= "</td>";
                    $retorno .= "</tr>";
                $retorno .= "</tfoot>";
            endif;

            foreach($data['registros'] as $reg => $val):

                $retorno .= "<tbody>";
                $retorno .= "<tr>";
                    $retorno .= "<td>".$val->cliente."</td>";
                    $retorno .= "<td>".$val->data."</td>";
                    $retorno .= "<td>".$val->produto."</td>";
                    $retorno .= "<td>".$val->quantidade."</td>";
                    $retorno .= "<td>".$val->valor."</td>";
                    $retorno .= "<td><a href='".base_url()."painel/financeiro_pecas/form/".$val->id."' class=\"edit\">editar</a></td>";
                    $retorno .= "<td><a href='".base_url()."painel/financeiro_pecas/excluir/".$val->id."' class=\"delete\">excluir</a></td>";
                $retorno .= "</tr>";
                $retorno .= "</tbody>";

            endforeach;

        $retorno .= "</table>";

        echo $retorno;
        //echo $this->db->last_query();
    }
}
?>
