<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Tecnicos extends CI_controller {

    function __construct() {
        parent::__construct();

        if(!$this->session->userdata('logged_in'))
            redirect('index.php//painel/');
    }


    function index() {
        $data['registros'] = $this->db->get('tecnicos')->result();

        $this->load->view('painel/common/header');
        $this->load->view('painel/common/menu');
        $this->load->view('painel/tecnicos', $data);
        $this->load->view('painel/common/footer');
    }

    function form($id = FALSE){
        if($id)
            $data['registro'] = $this->db->get_where('tecnicos', array('id' => $id))->result();
        else
            $data['registro'] = FALSE;

        $this->load->view('painel/common/header');
        $this->load->view('painel/common/menu');
        $this->load->view('painel/tecnicos_form', $data);
        $this->load->view('painel/common/footer');
    }

    function inserir(){
        $this->db->set('nome', $this->input->post('nome'));
        $this->db->insert('tecnicos');
        redirect('index.php/painel/tecnicos/');
    }

    function editar($id){
        $this->db->set('nome', $this->input->post('nome'));
        $this->db->where('id', $id);
        $this->db->update('tecnicos');
        redirect('index.php/painel/tecnicos/');
    }

    function excluir($id){
        $this->db->delete('tecnicos', array('id' => $id));
        redirect('index.php/painel/tecnicos/');
    }

}
?>
