<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Produtos extends CI_controller {

	var $header;

    function __construct() {
        parent::__construct();

        $this->header['topo'] = array(
			array(
				'imagem' => 'produtos-banner.jpg',
				'titulo' => 'Aplicação<br>adequada e <br>segura de <br>materiais e<br>técnicas',
				'texto'  => ''
			)
		);
    }

    function index(){
    	redirect('index.php/produtos/cabines');
    }

    function cabines() {
        $this->load->view('common/header', $this->header);
        $this->load->view('produtos/submenu');
        $this->load->view('produtos/cabines');
        $this->load->view('common/footer');
    }

    function comandos() {
        $this->load->view('common/header', $this->header);
        $this->load->view('produtos/submenu');
        $this->load->view('produtos/comandos');
        $this->load->view('common/footer');
    }

    function botoeiras() {
        $this->load->view('common/header', $this->header);
        $this->load->view('produtos/submenu');
        $this->load->view('produtos/botoeiras');
        $this->load->view('common/footer');
    }

    function portas() {
        $this->load->view('common/header', $this->header);
        $this->load->view('produtos/submenu');
        $this->load->view('produtos/portas');
        $this->load->view('common/footer');
    }

    function avulsas() {
        $this->load->view('common/header', $this->header);
        $this->load->view('produtos/submenu');
        $this->load->view('produtos/avulsas');
        $this->load->view('common/footer');
    }    
}
?>
