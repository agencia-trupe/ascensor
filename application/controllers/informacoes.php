<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Informacoes extends CI_controller {

	var $header;

    function __construct() {
        parent::__construct();

        $this->header['topo'] = array(
			array(
				'imagem' => 'infos-banner.jpg',
				'titulo' => 'Mantenha-se<br>informado',
				'texto'  => ''
			)
		);
    }

    function index() {

        $data['lista'] = $this->db->get('informacoes')->result();

        $this->load->view('common/header', $this->header);
        $this->load->view('informacoes/lista', $data);
        $this->load->view('common/footer');
    }

    function detalhes($slug = false){

        if(!$slug)
            redirect('index.php/informacoes/index');

        $query = $this->db->get_where('informacoes', array('slug' => $slug))->result();

        if(!isset($query[0]))
            redirect('index.php/informacoes/index');

        $data['detalhes'] = $query[0];

    	$this->load->view('common/header', $this->header);
        $this->load->view('informacoes/detalhes', $data);
        $this->load->view('common/footer');	
    }

}
?>
