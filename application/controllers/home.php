<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Home extends CI_controller {

    function __construct() {
        parent::__construct();
    }


    function index() {

    	$header['topo'] = array(
    		array(
    			'imagem' => 'home-banner1.jpg',
    			'titulo' => 'Plataformas elevatórias',
    			'texto'  => 'ACESSIBILIDADE EM EDIFICAÇÕES RESIDENCIAIS OU PÚBLICAS'
    		),
    		array(
    			'imagem' => 'home-banner2.jpg',
    			'titulo' => 'Manutenção e modernização de elevadores',
    			'texto'  => 'SEGURANÇA E VALORIZAÇÃO DE IMÓVEIS'
    		)
    	);

        $this->load->view('common/header', $header);
        $this->load->view('home');
        $this->load->view('common/footer');
    }


}
?>
