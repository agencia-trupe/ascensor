<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Home extends CI_controller {

    function __construct() {
        parent::__construct();

        $this->load->library('simplelogin_cliente');
    }


    function index() {

        if($this->simplelogin_cliente->login($this->input->post('clientes-login'), $this->input->post('clientes-senha'))){
            redirect('index.php/cliente/visitas');
        }else{
            redirect('index.php/home');
        }
        
    }

    function logout(){
        $this->simplelogin_cliente->logout();
        redirect('index.php/home');
    }

}
?>
