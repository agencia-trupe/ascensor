<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Financeiro extends CI_controller {

    var $meses = array(
        1 => 'JANEIRO',
        2 => 'FEVEREIRO',
        3 => 'MARÇO',
        4 => 'ABRIL',
        5 => 'MAIO',
        6 => 'JUNHO',
        7 => 'JULHO',
        8 => 'AGOSTO',
        9 => 'SETEMBRO',
        10 => 'OUTUBRO',
        11 => 'NOVEMBRO',
        12 => 'DEZEMBRO'
    );

    function __construct() {
        parent::__construct();

        if(!$this->session->userdata('logged_in_cliente'))
            redirect('index.php/home');
    }


    function index($mes = FALSE, $ano = FALSE) {

        if(!$mes)
            $mes = date('m');

        if(!$ano)
            $ano = date('Y');

        $data['valor_total'] = 0;

        $data['data'] = $this->meses[$mes].' '.$ano;
        $data['nome_cliente'] = $this->session->userdata('nome');
        $data['anos'] = $this->geraArrayAnos();

        $data['mensal'] = $this->db->where('MONTH(data_vencimento)', $mes)
                                    ->where('YEAR(data_vencimento)', $ano)
                                    ->where('cliente', $this->session->userdata('id'))
                                    ->get('valor_contratacao')->result();

        foreach($data['mensal'] as $key => $val){
            $data['valor_total'] += $val->valor;
            $val->valor = valor($val->valor, 'exibir');
            $val->data_vencimento = formataData($val->data_vencimento, 'mysql2br');
            if($val->data_pagamento)
                $val->data_pagamento = formataData($val->data_pagamento, 'mysql2br');
        }

        $data['pecas'] = $this->db->where('MONTH(data)', $mes)
                                    ->where('YEAR(data)', $ano)
                                    ->where('cliente', $this->session->userdata('id'))
                                    ->get('valor_pecas')->result();

        foreach($data['pecas'] as $key => $val){
            $data['valor_total'] += $val->valor;
            $val->valor = valor($val->valor, 'exibir');
            $val->data = formataData($val->data, 'mysql2br');
        }

        $data['servicos'] = $this->db->where('MONTH(data)', $mes)
                                    ->where('YEAR(data)', $ano)
                                    ->where('cliente', $this->session->userdata('id'))
                                    ->get('valor_servicos')->result();

        foreach($data['servicos'] as $key => $val){
            $data['valor_total'] += $val->valor;
            $val->valor = valor($val->valor, 'exibir');
            $val->data = formataData($val->data, 'mysql2br');
        }

        $data['valor_total'] = valor($data['valor_total'],'exibir');

        $this->load->view('common/header_clientes', $data);
        $this->load->view('cliente/financeiro', $data);
        $this->load->view('common/footer_clientes');
    }

    private function geraArrayAnos(){
        $ano_atual = date('Y');
        $mes_atual = date('m');

        $retorno = array();

        $ano_completo = array(1 => 'janeiro',2 => 'fevereiro',3 => 'março',4 => 'abril',5 => 'maio',
                                    6 => 'junho',7 => 'julho',8 => 'agosto',9 => 'setembro',10 => 'outubro',
                                    11 => 'novembro',12 => 'dezembro');

        $ano_incompleto = array();

        for($m = $mes_atual; $m >= 01; $m--){
            $ano_incompleto[$m] = nomeMes($m);
        }

        for($i = $ano_atual; $i > ($ano_atual - 5); $i--){
            if($i == $ano_atual){
                $retorno[$i] = $ano_incompleto;
            }else{
                $retorno[$i] = array_reverse($ano_completo, TRUE);
            }
        }

        return $retorno;
    }

}
?>
