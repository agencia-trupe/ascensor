<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Quemsomos extends CI_controller {

    function __construct() {
        parent::__construct();
    }


    function index() {

    	$header['topo'] = array(
    		array(
    			'imagem' => 'quemsomos-banner.jpg',
    			'titulo' => 'Equipe especializada, treinada e ágil<br> no atendimento',
    			'texto'  => ''
    		)
    	);

        $this->load->view('common/header', $header);
        $this->load->view('quemsomos');
        $this->load->view('common/footer');
    }


}
?>
