<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Servicos extends CI_controller {

	var $header;

    function __construct() {
        parent::__construct();

        $this->header['topo'] = array(
			array(
				'imagem' => 'servicos-banner.jpg',
				'titulo' => 'Qualidade, segurança e agilidade',
				'texto'  => ''
			)
		);
    }

    function index(){
    	redirect('index.php/servicos/manutencao');
    }

    function manutencao() {
        $this->load->view('common/header', $this->header);
        $this->load->view('servicos/submenu');
        $this->load->view('servicos/manutencao');
        $this->load->view('common/footer');
    }

    function modernizacao_tecnologica(){
    	$this->load->view('common/header', $this->header);
    	$this->load->view('servicos/submenu');
        $this->load->view('servicos/modernizacao_tecnologica');
        $this->load->view('common/footer');
    }

    function modernizacao_estetica(){
    	$this->load->view('common/header', $this->header);
    	$this->load->view('servicos/submenu');
        $this->load->view('servicos/modernizacao_estetica');
        $this->load->view('common/footer');
    }

    function laudos_e_vistorias(){
    	$this->load->view('common/header', $this->header);
    	$this->load->view('servicos/submenu');
        $this->load->view('servicos/laudos_e_vistorias');
        $this->load->view('common/footer');
    }

    function normas(){
    	$this->load->view('common/header', $this->header);
    	$this->load->view('servicos/submenu');
        $this->load->view('servicos/normas');
        $this->load->view('common/footer');
    }

    function acessibilidade(){
    	$this->load->view('common/header', $this->header);
    	$this->load->view('servicos/submenu');
        $this->load->view('servicos/acessibilidade');
        $this->load->view('common/footer');
    }

    function antigos(){
		$this->load->view('common/header', $this->header);
		$this->load->view('servicos/submenu');
        $this->load->view('servicos/antigos');
        $this->load->view('common/footer');    	
    }

}
?>
