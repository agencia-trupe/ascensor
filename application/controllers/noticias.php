<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Noticias extends CI_controller {

    function __construct() {
        parent::__construct();
    }


    function index() {
        $this->load->view('common/header');
        $this->load->view('noticias');
        $this->load->view('common/footer');
    }


}
?>
