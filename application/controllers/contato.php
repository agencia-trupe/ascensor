<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Contato extends CI_controller {

    function __construct() {
        parent::__construct();
    }


    function index() {

        if($this->session->flashdata('envio')){
           $data['envio'] = TRUE;
           $data['envio_status'] = $this->session->flashdata('envio_status');
        }else{
           $data['envio'] = FALSE;
        }

        $header['topo'] = array(
            array(
                'imagem' => 'contato-banner.jpg',
                'titulo' => 'Fale conosco<br>e tire suas<br>dúvidas',
                'texto'  => ''
            )
        );

        $this->load->view('common/header', $header);
        $this->load->view('contato', $data);
        $this->load->view('common/footer');
    }

    function envio(){
        $emailconf['charset'] = 'utf-8';
        $emailconf['mailtype'] = 'html';
        $emailconf['protocol'] = 'smtp';
        $emailconf['smtp_host'] = 'smtp.ascensorelevadores.com.br';
        $emailconf['smtp_user'] = 'noreply@ascensorelevadores.com.br';
        $emailconf['smtp_pass'] = 'norep2013';
        $emailconf['smtp_port'] = 587;
        $emailconf['crlf'] = "\r\n";
        $emailconf['newline'] = "\r\n";        
        
        $this->load->library('email');

        $this->email->initialize($emailconf);

        $u_nome = $this->input->post('nome');
        $u_email = $this->input->post('email');
        $u_fone = $this->input->post('telefone');
        $u_assun = $this->input->post('assunto');
        $u_msg = $this->input->post('mensagem');

        $from = 'noreply@ascensorelevadores.com.br'; // senha : norep2013
        $fromname = 'Ascensor Elevadores';
        $to = 'comercial@ascensorelevadores.com.br';
        $bc = FALSE;
        $bcc = 'bruno@trupe.net';

        $assunto = 'Email de Contato - Ascensor Elevadores';
        $email = "<html>
                    <head>
                        <style type='text/css'>
                            .tit{
                                font-weight:bold;
                                font-size:14px;
                                color:#5DB8C9;
                                font-family:Arial;
                            }
                            .val{
                                color:#000;
                                font-size:12px;
                                font-family:Arial;
                            }
                        </style>
                    </head>
                    <body>
                    <span class='tit'>Nome :</span> <span class='val'>$u_nome</span><br />
                    <span class='tit'>E-mail :</span> <span class='val'>$u_email</span><br />
                    <span class='tit'>Telefone :</span> <span class='val'>$u_fone</span><br />
                    <span class='tit'>Mensagem :</span> <span class='val'>$u_msg</span>
                    </body>
                    </html>";

        $this->email->from($from, $fromname);
        $this->email->to($to);
        
        if($bc)
            $this->email->bc($bc);
        if($bcc)
            $this->email->bcc($bcc);
        
        $this->email->reply_to($u_email);

        $this->email->subject($assunto);
        $this->email->message($email);

        $this->session->set_flashdata('envio_status', TRUE);

        switch (ENVIRONMENT){
            case 'development':
                die('Aplicacao rodando em servidor local. Sem SMTP.');
                return false;
                break;
            case 'testing':
                if(!$this->email->send()){
                    echo $this->email->print_debugger();
                    return false;
                }
                break;
            case 'production':
                if(!$this->email->send())
                    $this->session->set_flashdata('envio_status', FALSE);
                break;
        }

        $this->session->set_flashdata('envio', TRUE);
        redirect('index.php/contato');
    }

}
?>
