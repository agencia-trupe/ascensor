<?php

    function valor($valor, $tipo){
        if($tipo == 'banco'){
            $valor = str_replace('.', '', $valor);
            $valor = str_replace(',', '.', $valor);
            return (float) $valor * 100;
        }elseif($tipo == 'exibir'){
            $valor = $valor / 100;
            return number_format($valor, 2, ',', '.');
        }else{
            return $valor;
        }
    }

?>
