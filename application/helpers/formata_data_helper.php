<?php

function formataData($data, $tipo){
    if($tipo == 'br2mysql'){
        list($dia,$mes,$ano) = explode('/', $data);
        return $ano.'-'.$mes.'-'.$dia;
    }elseif($tipo == 'mysql2br'){
        list($ano,$mes,$dia) = explode('-', $data);
        return $dia.'/'.$mes.'/'.$ano;
    }
}

function formataHora($hora, $tipo){
    if($tipo == 'br2mysql'){
        return $hora.':00';
    }elseif($tipo == 'mysql2br'){
        list($h, $m, $s) = explode(':', $hora);
        return $h.':'.$m;
    }
}

function formataDataHora($str){
    list($data, $hora) = explode(' ', $str);
    $data = formataData($data,'mysql2br');
    $hora = formataHora($hora,'mysql2br');
    return $data.' ('.$hora.'h)';
}

function pegaMes($data, $formato, $extenso = TRUE){

    $meses = array(
        '01' => 'Janeiro',
        '02' => 'Fevereiro',
        '03' => 'Março',
        '04' => 'Abril',
        '05' => 'Maio',
        '06' => 'Junho',
        '07' => 'Julho',
        '08' => 'Agosto',
        '09' => 'Setembro',
        '10' => 'Outubro',
        '11' => 'Novembro',
        '12' => 'Dezembro'
    );

    if($formato == 'mysql'){
        list($ano,$mes,$dia) = explode('-',$data);
        return ($extenso) ? $meses[$mes] : $mes;
    }elseif($formato == 'br'){
        list($dia,$mes,$ano) = explode('/', $data);
        return ($extenso) ? $meses[$mes] : $mes;
    }else
        return $data;
}

function nomeMes($mes){
    $meses = array(
        1 => 'janeiro',
        2 => 'fevereiro',
        3 => 'março',
        4 => 'abril',
        5 => 'maio',
        6 => 'junho',
        7 => 'julho',
        8 => 'agosto',
        9 => 'setembro',
        10 => 'outubro',
        11 => 'novembro',
        12 => 'dezembro'
    );

    return $meses[$mes];
}

?>
