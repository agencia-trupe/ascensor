<script type="text/javascript">
    $(document).ready( function(){

        $('table').each( function(){
            $('td').each( function(){
                if(parseInt($(this).css('height')) > 30)
                    $(this).css('line-height', '150%');
            });
        });

    });
</script>

<h2>ACOMPANHAMENTO FINANCEIRO</h2>

<div class="financeiro">

    <div class="data"><?= $data ?></div>

    <h4>MANUTENÇÃO MENSAL</h4>

    <table class="tabela-financeiro total-mensal">

        <thead>
            <tr>
                <th class="col1">DESCRIÇÃO</th>
                <th class="col2">VENCIMENTO</th>
                <th class="col3">DATA DO PAGAMENTO</th>
                <th class="col4">VALOR</th>
            </tr>
        </thead>

        <tbody>
            <?if($mensal):?>
                <?foreach($mensal as $v_mensal):?>
                    <tr>
                        <td class="col1"><?= $v_mensal->descricao ?></td>
                        <td class="col2"><?= $v_mensal->data_vencimento ?></td>
                        <td class="col3"><?= $v_mensal->data_pagamento ?></td>
                        <td class="col4">R$<?= $v_mensal->valor?></td>
                    </tr>
                <?endforeach;?>
            <?else:?>
                    <tr>
                        <td colspan="4" class="empty-row">Nenhum valor cadastrado para este mês.</td>
                    </tr>
            <?endif;?>
        </tbody>

    </table>

    <h4>PEÇAS INSTALADAS</h4>

    <table class="tabela-financeiro total-pecas">

        <thead>
            <tr>
                <th class="col1">PRODUTO</th>
                <th class="col2">DESCRITIVO</th>
                <th class="col3">QTDE.</th>
                <th class="col4">DATA</th>
                <th class="col5">VALOR</th>
            </tr>
        </thead>

        <tbody>
            <?if($pecas):?>
                <?foreach($pecas as $v_pecas):?>
                    <tr>
                        <td class="col1"><?= $v_pecas->produto ?></td>
                        <td class="col2"><?= $v_pecas->descritivo ?></td>
                        <td class="col3"><?= $v_pecas->quantidade ?></td>
                        <td class="col4"><?= $v_pecas->data ?></td>
                        <td class="col5">R$<?=$v_pecas->valor?></td>
                    </tr>
                <?endforeach;?>
            <?else:?>
                    <tr>
                        <td colspan="5" class="empty-row">Nenhum valor cadastrado para este mês.</td>
                    </tr>
            <?endif;?>
        </tbody>

    </table>

    <h4>SERVIÇOS EXTRAS EXECUTADOS</h4>

    <table class="tabela-financeiro total-servicos">

        <thead>
            <tr>
                <th class="col1">SERVIÇO</th>
                <th class="col2">DESCRITIVO</th>
                <th class="col3">DATA</th>
                <th class="col4">VALOR</th>
            </tr>
        </thead>
        <tbody>
            <?if($servicos):?>
                <?foreach($servicos as $v_servicos):?>
                    <tr>
                        <td class="col1"><?= $v_servicos->servico ?></td>
                        <td class="col2"><?= $v_servicos->descritivo ?></td>
                        <td class="col3"><?= $v_servicos->data ?></td>
                        <td class="col4">R$ <?= $v_servicos->valor ?></td>
                    </tr>
                <?endforeach;?>
            <?else:?>
                <tr>
                    <td colspan="4" class="empty-row">Nenhum valor cadastrado para este mês.</td>
                </tr>
            <?endif;?>
        </tbody>

    </table>

    <div class="faixa-total">
        <span class="valor-total">R$ <?= $valor_total ?></span>
        TOTAL ATÉ O MOMENTO :
    </div>


    <div class="titulo-historico"><h2>CONSULTAR HISTÓRICO DE MESES ANTERIORES</h2></div>

    <div class="historico">

        <?foreach($anos as $ano => $meses):?>

            <div class="caixa-anual">

                <div class="hist-box ano"><?= $ano ?></div>

                <?foreach($meses as $n_mes => $mes):?>
                    <div class="hist-box"><a href="<?= base_url('index.php') ?>/cliente/financeiro/index/<?=$n_mes?>/<?=$ano?>"><?=$mes?></a></div>
                <?endforeach;?>

            </div>

        <?endforeach;?>
        
    </div>

</div>