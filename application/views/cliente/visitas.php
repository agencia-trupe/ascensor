<h2>ACOMPANHAMENTO DE VISITAS TÉCNICAS</h2>

<div class="visitas">

    <div class="data"><?= $data ?></div>

    <table class="tabela-visitas">

        <thead>
        <tr>
            <th class="col1">DATA</th>
            <th class="col2">OCORRÊNCIA</th>
            <th class="col3">TÉCNICO</th>
        </tr>
        </thead>

        <tbody>

            <?if($visitas):?>
                <?foreach($visitas as $visita):?>
                <tr>
                    <td class="col1"><?= $visita->data ?></td>
                    <td class="col2"><?= $visita->ocorrencia ?></td>
                    <td class="col3"><?= $visita->tecnico ?></td>
                </tr>
                <?endforeach;?>
            <?else:?>
                    <tr>
                        <td colspan="3" class="empty-row">Nenhum valor cadastrado para este mês.</td>
                    </tr>
            <?endif;?>

        </tbody>

    </table>

    <div class="titulo-historico"><h2>CONSULTAR HISTÓRICO DE MESES ANTERIORES</h2></div>

    <div class="historico">
        <?foreach($anos as $ano => $meses):?>

            <div class="caixa-anual">

                <div class="hist-box ano"><?= $ano ?></div>

                <?foreach($meses as $n_mes => $mes):?>
                    <div class="hist-box"><a href="<?= base_url() ?>index.php/cliente/visitas/index/<?=$n_mes?>/<?=$ano?>"><?= $mes ?></a></div>
                <?endforeach;?>
            
            </div>
        
        <?endforeach;?>
    </div>

</div>