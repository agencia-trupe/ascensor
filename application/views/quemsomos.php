<div class="barra-lateral">

    <h2>QUEM SOMOS</h2>
    Com experiência de 20 anos, executamos serviços de instalação e manutenção de elevadores com qualidade, eficiência e rapidez. Fornecemos produtos de alta tecnologia e confiabilidade.

</div>

<div class="conteudo">

    <h1 class="titulo-internas">A Empresa</h1>
    
    <div class="box-laranja-internas">
        <img src="_imgs/layout/quemsomos-img.jpg">
        <div class="texto">
            <h2>
                Nosso objetivo: Satisfação total do cliente<br>com segurança e responsabilidade
            </h2>
            <p>
                Para a Ascensor é muito importante atender a todas as expectativas do cliente sem nunca abandonar os conceitos de segurança e responsabilidade, prestando consultoria no atendimento das solicitações com base na grande experiência da empresa.
            </p>
        </div>
    </div>

    <div class="texto-internas">

        <p>
            A <strong>Ascensor Elevadores</strong> está situada estrategicamente na região central de São Paulo, para facilitar o atendimento a todas as regiões da cidade e da Grande São Paulo.        
        </p>

        <p>
            Oferecemos serviços e produtos de qualidade, sempre em busca da excelência. Nossa imagem projetada no mercado e junto aos clientes reflete esse esforço e mantém clientes fiéis por longos períodos.         
        </p>

        <p>
            Nosso objetivo é sempre garantir o melhor atendimento, com comprometimento, qualidade, rapidez no atendimento e a satisfação total do cliente.        
        </p>

        <h3>
            Equipe especializada        
        </h3>

        <p>
            Nossa equipe é composta por técnicos altamente qualificados, especialistas em elevadores de diversas marcas que realizam com perfeição e prontidão qualquer trabalho relacionado à área de transporte vertical.        
        </p>

        <p>
            Somos certificados e executamos serviços dentro dos padrões, normas e procedimentos técnicos do CONTRU, CREA e ABNT, garantindo segurança em todos os processos.        
        </p>

        <p>
            CONTRU Nº 000178/2010<br>
            CREA Nº  0900277
        </p>

        <div class="boxes-quemsomos">
            <div class="box">
                <div class="titulo">Infraestrutura Adequada</div>
                <img src="_imgs/layout/quemsomos-caixa1.jpg">
                <p>
                    Atendimento telefônico 24 horas, direcionando técnicos prontamente a atendimento de chamados.
                </p>
            </div>
            <div class="box">
                <div class="titulo">Equipe Treinada</div>
                <img src="_imgs/layout/quemsomos-caixa2.jpg">
                <p>
                    Técnicos prontos para solucionar problemas em elevadores de todas as marcas e modelos.
                </p>                
            </div>
            <div class="box">
                <div class="titulo">Experiência e Qualidade</div>
                <img src="_imgs/layout/quemsomos-caixa3.jpg">
                <p>
                    Maquinário para produção de ítens de segurança e de cabinas para elevador.
                </p>                
            </div>
        </div>

    </div>

</div>


<br clear="all">