<div id="content" class="container_16 clearfix">

    <div class="grid_16">
        <h2><? echo ($registro) ? 'Editar' : 'Adicionar';?> Visita Técnica</h2>
    </div>


    <?php
    if(isset($registro[0]) AND !empty($registro[0])):
    ?>

<form name="visitas-form" id="visitas-form" method="post" action="<?= base_url('index.php').'/' ?>painel/visitas/editar/<?= $registro[0]->id ?>">

    Cliente<br />
    <select name="cliente" id="cliente">
        <option value=""></option>
        <? foreach($clientes as $reg): ?>
            <option value="<?=$reg->id?>" <?if($reg->id == $registro[0]->cliente) echo 'selected'?>><?=$reg->nome?></option>
        <? endforeach;?>
    </select><br/><br/>

    Data<br />
    <input type="text" name="data" autocomplete="off" maxlength="10" id="data" class="datepicker" value="<?=$registro[0]->data['data']?>" /><br /><br />

    Horário<br/>
    <input type="text" name="horario" autocomplete="off" maxlength="5" id="horario" class="timepicker" value="<?=$registro[0]->data['hora']?>"/><br/><br/>

    Técnico<br />
    <select name="tecnico" id="tecnico">
        <option value=""></option>
        <? foreach($tecnicos as $reg): ?>
            <option value="<?=$reg->id?>" <?if($reg->id == $registro[0]->tecnico) echo 'selected'?>><?=$reg->nome?></option>
        <? endforeach;?>
    </select><br/><br/>

    Ocorrência<br />
    <textarea name="ocorrencia" id="tinymce"><?=$registro[0]->ocorrencia?></textarea><br /><br />


    <input type="submit" value="Gravar" /> <input type="button" value="Voltar" class="btn-voltar" />
</form>

<?php else: ?>

<form name="visitas-form" id="visitas-form" method="post" action="<?= base_url('index.php').'/' ?>painel/visitas/inserir/">

    Cliente<br />
    <select name="cliente" id="cliente">
        <option value=""></option>
        <? foreach($clientes as $reg): ?>
            <option value="<?=$reg->id?>"><?=$reg->nome?></option>
        <? endforeach;?>
    </select><br/><br/>

    Data<br />
    <input type="text" name="data" autocomplete="off" maxlength="10"  id="data" class="datepicker" /><br /><br />

    Horário<br/>
    <input type="text" name="horario" autocomplete="off" maxlength="5" id="horario" class="timepicker" /><br/><br/>

    Técnico<br />
    <select name="tecnico" id="tecnico">
        <option value=""></option>
        <? foreach($tecnicos as $reg): ?>
            <option value="<?=$reg->id?>"><?=$reg->nome?></option>
        <? endforeach;?>
    </select><br/><br/>

    Ocorrência<br />
    <textarea name="ocorrencia" id="tinymce"></textarea><br /><br />
    
    <input type="submit" value="Gravar" /> <input type="button" value="Voltar" class="btn-voltar" />
</form>

<?php endif; ?>

</div>

<!---------------------------------------------------------->

