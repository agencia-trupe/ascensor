<div id="content" class="container_16">

    <div class="grid_16">
        <h2>Técnicos</h2>
    </div>

    <div class="clearfix"></div>

    <a href="<?= base_url('index.php').'/' ?>painel/tecnicos/form" class="add">Adicionar Técnico <img src="<?= base_url() ?>_imgs/painel/add-icon.png"></a><br/><br/>

    <table>
    <thead>
        <tr>
            <th>Nome</th>
            <th></th>
            <th></th>
        </tr>
    </thead>

    <?if(isset($paginacao)):?>
    <tfoot>
        <tr>
            <td colspan="3" class="pagination">
<!--                <span class="active curved">1</span><a href="#" class="curved">2</a><a href="#" class="curved">3</a><a href="#" class="curved">4</a> ... <a href="#" class="curved">10 million</a>-->
                <?=$paginacao?>
            </td>
        </tr>
    </tfoot>
    <?endif;?>

    <tbody>
<?php
if(!empty($registros)){
    foreach($registros as $value){
        echo '<tr>';
        echo "<td>".$value->nome."</td>";
        echo "<td><a class='edit' href='".base_url('index.php').'/'."painel/tecnicos/form/".$value->id."'>editar</a></td>";
        echo "<td><a class='delete' href='".base_url('index.php').'/'."painel/tecnicos/excluir/".$value->id."'>excluir</a></td>";

        echo "</tr>";
    }
}else{
    echo "<tr><td colspan='3'><h3>Nenhum técnico cadastrado</h3></td></tr>";
}
?>
    </tbody>
    </table>


</div>