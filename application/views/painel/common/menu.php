<h1 id="head">Painel de Controle | <?=CLIENTE?></h1>

<ul id="navigation">
        <li><a href="<?= base_url('index.php').'/' ?>painel/home/">Home</a></li>
        <li><a href="<?= base_url('index.php').'/' ?>painel/noticias">Informações Úteis</a></li>
        <li><a href="<?= base_url('index.php').'/' ?>painel/visitas">Visitas Técnicas</a></li>
        <li><a href="" class="comsubmenu">Acompanhamento Financeiro</a>
            <ul class="submenu sub-financeiro">
                <li><a href="<?= base_url('index.php').'/' ?>painel/financeiro_mensal">Mensal (contratação)</a></li>
                <li><a href="<?= base_url('index.php').'/' ?>painel/financeiro_pecas">Peças</a></li>
                <li><a href="<?= base_url('index.php').'/' ?>painel/financeiro_servicos">Serviços</a></li>
                <li><a href="<?= base_url('index.php').'/' ?>painel/financeiro">Total</a></li>
            </ul>
        </li>

        <li><a href="" class="comsubmenu">Cadastros</a>
            <ul class="submenu sub-cadastros">
                <li><a href="<?= base_url('index.php').'/' ?>painel/clientes">Clientes</a></li>
                <li><a href="<?= base_url('index.php').'/' ?>painel/tecnicos">Técnicos</a></li>
                <li><a href="<?= base_url('index.php').'/' ?>painel/usuarios">Usuários do Painel</a></li>
            </ul>
        </li>
        
        <li id="logout-btn"><a href="<?= base_url('index.php').'/' ?>painel/home/logout">Logout</a></li>
</ul>