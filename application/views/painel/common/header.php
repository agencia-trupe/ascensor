<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title>Painel de Controle | <?=CLIENTE?></title>
                <link rel="stylesheet" href="<?= base_url() ?>css/tpl/960.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="<?= base_url() ?>css/tpl/template.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="<?= base_url() ?>css/tpl/colour.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="<?= base_url() ?>css/smoothness/jquery-ui-1.8.12.custom.css" type="text/css" media="screen" charset="utf-8" />

                <script type="text/javascript">
                    var CI_ROOT = '<?= base_url()?>';
                </script>

                <script type="text/javascript" src="<?= base_url() ?>js/jquery-1.6.4.min.js"></script>
                <script type="text/javascript" src="<?= base_url() ?>js/jquery-ui-1.8.12.custom.min.js"></script>
                <script type="text/javascript" src="<?= base_url() ?>js/jquery.ui.datepicker-pt-BR.js"></script>
                <script type='text/javascript' src='<?= base_url() ?>js/jquery-ui-timepicker-addon.js'></script>
                <script type="text/javascript" src="<?= base_url() ?>js/tinymce/tiny_mce.js"></script>
                
                <script src="<?= base_url() ?>js/glow/1.7.0/core/core.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>js/glow/1.7.0/widgets/widgets.js" type="text/javascript"></script>
		<link href="<?= base_url() ?>js/glow/1.7.0/widgets/widgets.css" type="text/css" rel="stylesheet" />
                
                <script type="text/javascript" src="<?= base_url() ?>js/back.js"></script>
	</head>
	<body>