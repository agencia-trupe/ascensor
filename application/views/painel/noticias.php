<div id="content" class="container_16 clearfix">

    <div class="grid_16">
            <h2>Informações Úteis</h2>
<!--            <p class="error">Something went wrong.</p>-->
    </div>

    <div class="grid_16">

        <a href="<?= base_url('index.php').'/' ?>painel/noticias/form" class="add">Adicionar item <img src="<?= base_url() ?>_imgs/painel/add-icon.png"></a><br/><br/>

        <? if($registros): ?>

        <table>
            <tr>
                <th>Título</th>
                <th>Data</th>
                <th></th>
                <th></th>
            </tr>

            <? foreach($registros as $reg):?>
                <tr>
                    <td><?=$reg->titulo?></td>
                    <td><?=word_limiter($reg->texto, 15)?></td>
                    <td><a href="<?= base_url('index.php').'/' ?>painel/noticias/form/<?=$reg->id?>" class="edit">editar</a></td>
                    <td><a href="<?= base_url('index.php').'/' ?>painel/noticias/excluir/<?=$reg->id?>" class="delete">excluir</a></td>
                </tr>
            <? endforeach;?>

        </table>

        <? else: ?>

            <h2>Nenhum Item Cadastrado</h2>

        <? endif; ?>

    </div>

</div>