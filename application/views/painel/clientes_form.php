<div id="content" class="container_16 clearfix">

    <div class="grid_16">
        <h2><? echo ($registro) ? 'Editar' : 'Adicionar';?> Cliente</h2>
    </div>


    <?php
    if(isset($registro[0]) AND !empty($registro[0])):
    ?>

<form name="clientes-form" id="clientes-form" method="post" action="<?= base_url('index.php').'/' ?>painel/clientes/editar/<?= $registro[0]->id ?>">
    Nome<br />
    <input type="text" name="nome" id="nome" value="<?= $registro[0]->nome ?>"/><br /><br />
    Email<br />
    <input type="text" name="email"  value="<?= $registro[0]->email ?>"/><br /><br />
    Telefone 1<br />
    <input type="text" name="telefone1"  value="<?= $registro[0]->telefone1 ?>"/><br /><br />
    Telefone 2<br />
    <input type="text" name="telefone2"  value="<?= $registro[0]->telefone2 ?>"/><br /><br />
    Endereço<br />
    <input type="text" name="endereco"  value="<?= $registro[0]->endereco ?>"/><br /><br />
    Usuário para acesso ao sistema<br/>
    <input type="text" name="username" id="username"  value="<?= $registro[0]->username ?>"/><br /><br />
    Senha para acesso ao sistema <small>(deixe em branco para manter a senha atual)</small><br/>
    <input type="password" name="password" id="password"/><br /><br />
    Confirme a Senha<br />
    <input type="password" name="senha_conf" id="senha_conf" /><br /><br />
    <input type="hidden" id="edicao">

    <input type="submit" value="Gravar" /> <input type="button" value="Voltar" class="btn-voltar" />
</form>

<?php else: ?>

<form name="clientes-form" id="clientes-form" method="post" action="<?= base_url('index.php').'/' ?>painel/clientes/inserir/">
    Nome<br />
    <input type="text" name="nome" id="nome" /><br /><br />
    Email<br />
    <input type="text" name="email" /><br /><br />
    Telefone 1<br />
    <input type="text" name="telefone1" /><br /><br />
    Telefone 2<br />
    <input type="text" name="telefone2" /><br /><br />
    Endereço<br />
    <input type="text" name="endereco" /><br /><br />
    Usuário para acesso ao sistema<br/>
    <input type="text" name="username" id="username" /><br /><br />
    Senha para acesso ao sistema<br/>
    <input type="password" name="password" id="password"/><br /><br />
    Confirme a Senha<br />
    <input type="password" name="senha_conf" id="senha_conf" /><br /><br />
    <input type="submit" value="Gravar" /> <input type="button" value="Voltar" class="btn-voltar" />
</form>

<?php endif; ?>

</div>

<!---------------------------------------------------------->