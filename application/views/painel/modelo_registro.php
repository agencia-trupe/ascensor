<div id="content" class="container_16 clearfix">
    <div class="grid_16">
            <h2>Notícias</h2>
<!--            <p class="error">Something went wronk.</p>-->
    </div>

    <div class="grid_16">

        <? if($registros): ?>

        <table>
            <tr>
                <th>Título</th>
                <th>Data</th>
                <th></th>
                <th></th>
            </tr>

            <? foreach($registros as $reg):?>
                <tr>
                    <td><?=$reg->titulo?></td>
                    <td><?=formataData($reg->data, 'mysql2br')?></td>
                    <td>editar</td>
                    <td>excluir</td>
                </tr>
            <? endforeach;?>

        </table>

        <? else: ?>

            <h2>Nenhuma Notícia Cadastrada</h2>

        <? endif; ?>
            
    </div>

</div>