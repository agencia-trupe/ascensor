<div id="content" class="container_16 clearfix">

    <div class="grid_16">
        <h2><? echo ($registro) ? 'Editar' : 'Adicionar';?> Valor Mensal de Serviços</h2>
    </div>


    <?php
    if(isset($registro[0]) AND !empty($registro[0])):
    ?>

<form name="servicos-form" id="servicos-form" method="post" action="<?= base_url('index.php').'/' ?>painel/financeiro_servicos/editar/<?= $registro[0]->id ?>">

    Cliente<br />
    <select name="cliente" id="cliente">
        <option value=""></option>
        <? foreach($clientes as $reg): ?>
            <option value="<?=$reg->id?>" <?if($reg->id == $registro[0]->cliente) echo ' selected'?>><?=$reg->nome?></option>
        <? endforeach;?>
    </select><br/><br/>

    Data<br />
    <input type="text" name="data" autocomplete="off" maxlength="10"  id="data" class="datepicker" value="<?= formataData($registro[0]->data,'mysql2br') ?>" /><br /><br />

    Serviço<br/>
    <input type="text" name="servico" maxlength="140" id="servico" value="<?= $registro[0]->servico ?>"/><br /><br />

    Valor (R$)<br/>
    <input type="text" name="valor" autocomplete="off" id="valor" value="<?= valor($registro[0]->valor,'exibir') ?>" /><br/><br/>

    Descritivo<br />
    <textarea name="descricao" id="tinymce"><?= $registro[0]->descritivo ?></textarea><br /><br />

    <input type="submit" value="Gravar" /> <input type="button" value="Voltar" class="btn-voltar" />
</form>

<?php else: ?>

<form name="servicos-form" id="servicos-form" method="post" action="<?= base_url('index.php').'/' ?>painel/financeiro_servicos/inserir/">

    Cliente<br />
    <select name="cliente" id="cliente">
        <option value=""></option>
        <? foreach($clientes as $reg): ?>
            <option value="<?=$reg->id?>"><?=$reg->nome?></option>
        <? endforeach;?>
    </select><br/><br/>

    Data<br />
    <input type="text" name="data" autocomplete="off" maxlength="10"  id="data" class="datepicker" /><br /><br />

    Serviço<br/>
    <input type="text" name="servico" maxlength="140" id="servico" /><br /><br />

    Valor (R$)<br/>
    <input type="text" name="valor" autocomplete="off" id="valor" /><br/><br/>

    Descritivo<br />
    <textarea name="descricao" id="tinymce"></textarea><br /><br />

    <input type="submit" value="Gravar" /> <input type="button" value="Voltar" class="btn-voltar" />
</form>

<?php endif; ?>

</div>

<!---------------------------------------------------------->

