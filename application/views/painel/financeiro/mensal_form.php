<div id="content" class="container_16 clearfix">

    <div class="grid_16">
        <h2><? echo ($registro) ? 'Editar' : 'Adicionar';?> Valor Mensal de Contratação</h2>
    </div>


    <?php
    if(isset($registro[0]) AND !empty($registro[0])):
    ?>

<form name="mensal-form" id="mensal-form" method="post" action="<?= base_url('index.php').'/' ?>painel/financeiro_mensal/editar/<?= $registro[0]->id ?>">

    Cliente<br />
    <select name="cliente" id="cliente">
        <option value=""></option>
        <? foreach($clientes as $reg): ?>
            <option value="<?=$reg->id?>" <?if($reg->id == $registro[0]->cliente) echo ' selected'?>><?=$reg->nome?></option>
        <? endforeach;?>
    </select><br/><br/>

    Data Vencimento<br />
    <input type="text" name="data_vencimento" autocomplete="off" maxlength="10"  id="data_vencimento" class="datepicker" value="<?=formataData($registro[0]->data_vencimento, 'mysql2br')?>"/><br /><br />

    Valor (R$)<br/>
    <input type="text" name="valor" autocomplete="off" maxlength="20" id="valor" value="<?=valor($registro[0]->valor, 'exibir')?>"/><br/><br/>

    Observações<br />
    <textarea name="descricao" id="tinymce"><?=$registro[0]->descricao?></textarea><br /><br />

    <? if($registro[0]->data_pagamento): ?>

        <label>PAGO <input type="checkbox" value="1" name="pago" id="valor_pago" checked></label>
        <div class="data_pagamento">
            Data de Pagamento<br/>
            <input type="text" name="data_pagamento" maxlength="10" class="datepicker" id="data_pagamento" value="<?=$reg->data_pagamento?>">
        </div>

    <? else: ?>

        <label>PAGO <input type="checkbox" value="1" name="pago" id="valor_pago"></label>
        <div class="data_pagamento hid">
            Data de Pagamento<br/>
            <input type="text" name="data_pagamento" maxlength="10" class="datepicker" id="data_pagamento">
        </div>
        
    <? endif; ?>

    <br /><br />
    <input type="submit" value="Gravar" /> <input type="button" value="Voltar" class="btn-voltar" />
</form>

<?php else: ?>

<form name="mensal-form" id="mensal-form" method="post" action="<?= base_url('index.php').'/' ?>painel/financeiro_mensal/inserir/">

    Cliente<br />
    <select name="cliente" id="cliente">
        <option value=""></option>
        <? foreach($clientes as $reg): ?>
            <option value="<?=$reg->id?>"><?=$reg->nome?></option>
        <? endforeach;?>
    </select><br/><br/>

    Data Vencimento<br />
    <input type="text" name="data_vencimento" autocomplete="off" maxlength="10"  id="data_vencimento" class="datepicker" /><br /><br />

    Valor (R$)<br/>
    <input type="text" name="valor" maxlength="20" autocomplete="off" id="valor" /><br/><br/>

    Observações<br />
    <textarea name="descricao" id="tinymce"></textarea><br /><br />

    <input type="submit" value="Gravar" /> <input type="button" value="Voltar" class="btn-voltar" />
</form>

<?php endif; ?>

</div>

<!---------------------------------------------------------->

