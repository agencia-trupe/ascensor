<div id="content" class="container_16 clearfix">

    <div class="grid_16">
            <h2>Acompanhamento Financeiro - Total Mensal</h2>
    </div>

    <div class="grid_16">

        <? if($cliente): ?>

        <h3>Selecione o cliente</h3>

        <select name="cliente" id="select-cliente-total">
            <option value=""></option>
            <? foreach($clientes as $cli): ?>
                <option value="<?=$cli->id?>" <?if($cli->id == $cliente) echo 'selected'?>><?=$cli->nome?></option>
            <? endforeach; ?>
        </select><br/><br/>

        <div class="target">

        <table>
            <thead>
            <tr>
                <th>Cliente</th>
                <th>Valor</th>
                <th>Mês</th>
                <th>Data Vencimento</th>
                <th>Pago</th>
                <th></th>
                <th></th>
            </tr>
            </thead>

            <? if($paginacao): ?>
            <tfoot>
                <tr>
                    <td colspan="6" class="pagination">
                        <?=$paginacao?>
                    </td>
                </tr>
            </tfoot>
            <? endif; ?>

            <? foreach($registros as $reg):?>
                <tr>
                    <td><?=$reg->cliente?></td>
                    <td><?=$reg->valor?></td>
                    <td><?=$reg->mes?></td>
                    <td><?=formataData($reg->data_vencimento, 'mysql2br')?></td>
                    <td><? echo ($reg->data_pagamento) ? "PAGO (".formataData($reg->data_pagamento, 'mysql2br').")" : ""?></td>
                    <td><a href="<?= base_url('index.php').'/' ?>painel/financeiro_mensal/form/<?=$reg->id?>" class="edit">editar</a></td>
                    <td><a href="<?= base_url('index.php').'/' ?>painel/financeiro_mensal/excluir/<?=$reg->id?>" class="delete">excluir</a></td>
                </tr>
            <? endforeach;?>

        </table>

        <? else: ?>

            <h3>Selecione o cliente</h3>

            <select name="cliente" id="select-cliente-total">
                <option value=""></option>
                <? foreach($clientes as $cli): ?>
                    <option value="<?=$cli->id?>"><?=$cli->nome?></option>
                <? endforeach; ?>
            </select>

            <div id="target-mes">
                <br/>
                <h4>Selecione o mês</h4>

                <select name="mes" id="select-mes-total">
                    <option value=""</option>
                    <option value="01">Janeiro <?=Date('Y')?></option>
                    <option value="02">Fevereiro <?=Date('Y')?></option>
                    <option value="03">Março <?=Date('Y')?></option>
                    <option value="04">Abril <?=Date('Y')?></option>
                    <option value="05">Maio <?=Date('Y')?></option>
                    <option value="06">Junho <?=Date('Y')?></option>
                    <option value="07">Julho <?=Date('Y')?></option>
                    <option value="08">Agosto <?=Date('Y')?></option>
                    <option value="09">Setembro <?=Date('Y')?></option>
                    <option value="10">Outubro <?=Date('Y')?></option>
                    <option value="11">Novembro <?=Date('Y')?></option>
                    <option value="12">Dezembro <?=Date('Y')?></option>
                </select>
            </div>

            <div class="target">

        <? endif; ?>

        </div>
    </div>

</div>