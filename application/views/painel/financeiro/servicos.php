<div id="content" class="container_16 clearfix">

    <div class="grid_16">
            <h2>Acompanhamento Financeiro - Valor Mensal de Serviços</h2>
    </div>

    <div class="grid_16">

        <a href="<?= base_url('index.php').'/' ?>painel/financeiro_servicos/form" class="add">Adicionar Valor de Serviços<img src="<?= base_url() ?>_imgs/painel/add-icon.png"></a><br/><br/>

        <? if($cliente): ?>

        <h3>Selecione o cliente</h3>

        <select name="cliente" id="select-cliente-servicos">
            <option value=""></option>
            <? foreach($clientes as $cli): ?>
                <option value="<?=$cli->id?>" <?if($cli->id == $cliente) echo 'selected'?>><?=$cli->nome?></option>
            <? endforeach; ?>
        </select><br/><br/>

        <div class="target">

        <table>
            <thead>
            <tr>
                <th>Cliente</th>
                <th>Data</th>
                <th>Serviço</th>
                <th>Valor (R$)</th>
                <th></th>
                <th></th>
            </tr>
            </thead>

            <? if($paginacao): ?>
            <tfoot>
                <tr>
                    <td colspan="6" class="pagination">
                        <?=$paginacao?>
                    </td>
                </tr>
            </tfoot>
            <? endif; ?>

            <? foreach($registros as $reg):?>
                <tr>
                    <td><?=$reg->cliente?></td>
                    <td><?=formataData($reg->data, 'mysql2br')?></td>
                    <td><?=$reg->servico?></td>
                    <td><?=$reg->valor?></td>
                    <td><a href="<?= base_url('index.php').'/' ?>painel/financeiro_servicos/form/<?=$reg->id?>" class="edit">editar</a></td>
                    <td><a href="<?= base_url('index.php').'/' ?>painel/financeiro_servicos/excluir/<?=$reg->id?>" class="delete">excluir</a></td>
                </tr>
            <? endforeach;?>

        </table>

        <? else: ?>

            <h3>Selecione o cliente</h3>

            <select name="cliente" id="select-cliente-servicos">
                <option value=""></option>
                <? foreach($clientes as $cli): ?>
                    <option value="<?=$cli->id?>"><?=$cli->nome?></option>
                <? endforeach; ?>
            </select>

            <div class="target">

        <? endif; ?>

        </div>
    </div>

</div>