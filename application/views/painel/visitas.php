<div id="content" class="container_16 clearfix">

    <div class="grid_16">
            <h2>Visitas Técnicas</h2>
<!--            <p class="error">Something went wrong.</p>-->
    </div>

    <div class="grid_16">

        <a href="<?= base_url('index.php').'/' ?>painel/visitas/form" class="add">Adicionar Visita Técnica <img src="<?= base_url() ?>_imgs/painel/add-icon.png"></a><br/><br/>

        <div class="target">

        <? if($registros): ?>

        <h3>Selecione o cliente</h3>

        <select name="cliente" id="select-cliente-visita">
            <option value=""></option>
            <? foreach($clientes as $cli): ?>
                <option value="<?=$cli->id?>" <?if($cli->id == $cliente) echo 'selected'?>><?=$cli->nome?></option>
            <? endforeach; ?>
        </select><br/><br/>

        <table>
            <thead>
            <tr>
                <th>Cliente</th>
                <th>Data</th>
                <th>Descrição</th>
                <th></th>
                <th></th>
            </tr>
            </thead>

            <? if($paginacao): ?>
            <tfoot>
                <tr>
                    <td colspan="5" class="pagination">
                        <?=$paginacao?>
                    </td>
                </tr>
            </tfoot>
            <? endif; ?>

            <? foreach($registros as $reg):?>
                <tr>
                    <td><?=$reg->cliente?></td>
                    <td><?=$reg->data['data']?></td>
                    <td><? echo (strlen($reg->ocorrencia) > 30) ? substr($reg->ocorrencia, 0, 30).'...' : $reg->ocorrencia ?></td>
                    <td><a href="<?= base_url('index.php').'/' ?>painel/visitas/form/<?=$reg->id?>" class="edit">editar</a></td>
                    <td><a href="<?= base_url('index.php').'/' ?>painel/visitas/excluir/<?=$reg->id?>" class="delete">excluir</a></td>
                </tr>
            <? endforeach;?>

        </table>

        <? else: ?>

            <h3>Selecione o cliente</h3>

            <select name="cliente" id="select-cliente-visita">
                <option value=""></option>
                <? foreach($clientes as $cli): ?>
                    <option value="<?=$cli->id?>"><?=$cli->nome?></option>
                <? endforeach; ?>
            </select>

        <? endif; ?>

        </div>
    </div>

</div>