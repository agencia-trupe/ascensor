<div class="conteudo">

    <h1 class="titulo-internas">Botoeiras</h1>
    
    <div class="texto-internas">

    	<img src="_imgs/layout/produtos-botoeiras1.jpg" alt="Botoeiras de elevadores" class="fleft">
        <img src="_imgs/layout/produtos-botoeiras2.jpg" alt="Botoeiras de elevadores" class="fleft">

        <p>
            As atuais botoeiras de cabine e de pavimento completam a modernização e a exigência do Contru. Executadas em modelo aço inox escovado com ou sem IPD (indicador de posição digital), com botões auto-iluminados, braile.
        </p>
        <p>
            Observações:
        </p>
        <p>
            Braille: Placas de sinalização para utilização por deficientes visuais, é uma exigência da Prefeitura Municipal de São Paulo, conforme lei nº 11.858.
        </p>
        <p>
            As botoeiras de cabines podem ou não ter integrado o sistema intercomunicador (viva voz) para comunicação com a recepção da portaria.
        </p>

    </div>

</div>

<br clear="all">