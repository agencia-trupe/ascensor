<div class="conteudo">

    <h1 class="titulo-internas">Cabines</h1>
    
    <div class="texto-internas">

    	<img src="_imgs/layout/produtos-cabines1.jpg" alt="Cabines de elevadores" class="fleft">
        <img src="_imgs/layout/produtos-cabines2.jpg" alt="Cabines de elevadores" class="fleft">

        <p>
        	Modernização de cabines com materiais atuais, modernos e sofisticados, como:
        </p>

    	<ul>
			<li>aço inox escovado</li>
			<li>aço inox polido</li>
			<li>fórmica texturizada</li>
			<li>lâminas de madeiras diversas</li>
		</ul>        

    </div>

</div>

<br clear="all">