<div class="conteudo">

    <h1 class="titulo-internas">Comandos</h1>
    
    <div class="texto-internas">

    	<img src="_imgs/layout/produtos-comandos1.jpg" alt="Comandos de elevadores" class="fleft">
        <img src="_imgs/layout/produtos-comandos2.jpg" alt="Comandos de elevadores" class="fleft">

        <p>
        	Modernos quadros de comando computadorizados transformam elevadores antigos, com baixa segurança em máquinas inteligentes, mais ágeis, que consomem menos energia e fazem as viagens mais confortáveis e seguras.
        </p>

    	<p>
            QUADROS DE COMANDO COMPUTADORIZADOS:<br>
            <img src="_imgs/layout/produtos-comandos3.jpg" alt="Comandos de elevadores" style="margin-top:5px;">   
        </p>

    </div>

</div>

<br clear="all">