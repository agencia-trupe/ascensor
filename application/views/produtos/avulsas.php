<div class="conteudo">

    <h1 class="titulo-internas">Peças avulsas</h1>
    
    <div class="texto-internas">

    	<img src="_imgs/layout/produtos-pecasavulsas1.jpg" alt="Peças avulsas" class="fleft">
        <img src="_imgs/layout/produtos-pecasavulsas2.jpg" alt="Peças avulsas" class="fleft">

        <p>
        	A Ascensor provê peças avulsas para levadores de todas as marcas, tais como:
        </p>

    	<ul>
			<li>Mola de Porta Dorma</li>
            <li>Operador de Porta</li>
            <li>Ditador Schindler</li>
            <li>Limitador de Velocidade</li>
            <li>Cabos de Aço</li>
            <li>Sistema de seletor com sensor magnético (este sistema envia ao comando os sinais de posição da cabine fazendo com isso que o equipamento trabalhe com perfeição nas paradas do elevador)</li>
            <li>Produtos para Reformas de Portas e Batentes</li>
            <li>Peças para Modernização Estética</li>
		</ul>        

    </div>

</div>

<br clear="all">