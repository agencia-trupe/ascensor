</div> <!-- Fim da div centro -->

<? if($this->router->class != 'home'): ?>
<div class="barra-inf">

    <a href="index.php/servicos/modernizacao_tecnologica" title="Modernização Tecnológica" class="chamada-internas tecnica">
        MODERNIZAÇÃO TECNOLÓGICA
        <div class="saiba-mais">saiba mais &raquo;</div>
    </a>
    <a href="index.php/servicos/modernizacao_estetica" title="Modernização Estética" class="chamada-internas estetica">
        MODERNIZAÇÃO ESTÉTICA
        <div class="saiba-mais">saiba mais &raquo;</div>
    </a>
    <a href="index.php/servicos/manutencao" title="Manutenção de Elevadores" class="chamada-internas manutencao">
        MANUTENÇÃO DE ELEVADORES
        <div class="saiba-mais">saiba mais &raquo;</div>
    </a>

    <div class="barra-texto">
        Agende uma visita <br>
        de nossos consultores<br>
        e solicite seu orçamento.<br>
        <img src="_imgs/layout/home-crea.jpg" alt="CREA" class="selos prim">
        <img src="_imgs/layout/home-contru.jpg" alt="Contru" class="selos">
    </div>

</div>
<? endif; ?>

<div class="rodape">

    <div class="rodape-box prim">
        <strong>11 3104-0029</strong><br/>
        <strong>11 2843-6005 [24h]</strong><br/><br/>
        Rua do Bosque, 327<br/>
        Barra Funda<br/>
        São Paulo, SP<br/>
        01136-000
    </div>
    <div class="rodape-box">
        <ul class="menu-rodape">
            <li><a href="index.php/quemsomos" title="Quem Somos">QUEM SOMOS</a></li>
            <li><a href="index.php/servicos" title="Serviços">SERVIÇOS</a></li>
            <li><a href="index.php/produtos" title="Produtos">PRODUTOS</a></li>
            <li><a href="index.php/acessibilidade" title="Acessibilidade">ACESSIBILIDADE</a></li>
            <li><a href="index.php/informacoes" title="Informações Úteis">INFORMAÇÕES ÚTEIS</a></li>
            <li><a href="index.php/contato" title="Contato">CONTATO</a></li>
        </ul>
    </div>
    <div class="assinatura">
        &copy; <?= date('Y') ?> Ascensor Elevadores<br/>
        Todos os direitos reservados<br/><br/>
        <a href="http://www.trupe.net" title="Criação de Sites : Trupe Agência Criativa" target="_blank">Criação de Sites</a> : <a href="http://www.trupe.net" title="Criação de Sites : Trupe Agência Criativa" target="_blank">Trupe Agência Criativa</a>
    </div>


</div>

</div> <!-- fim da div principal -->

<?JS(array('jquery.corner', 'placeholder','cycle','front'))?>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-11772066-1");
pageTracker._trackPageview();
} catch(err) {}</script>

</body>
</html>