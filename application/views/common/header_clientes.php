<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd" >
<html>
    <head>
        <title>Ascensor Elevadores · Instalação e manutenção de elevadores, São Paulo - SP</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <META name="TITLE" content="Manutenção de elevadores. Instalação de elevadores. Reforma de elevadores. Modernização de Elevadores. Embelezamento de elevadores. São Paulo - SP">
        <META name="SUBJECT" content="Manutenção, instalação, reforma, modernização e embelezamento de elevadores. São Paulo - SP.">
        <meta name="Description" content="Manutenção de elevadores. Instalação de elevadores. Reforma de elevadores. Modernização de Elevadores. Embelezamento de elevadores. São Paulo - SP">
        <meta name="keywords" content="manutenção de elevadores, instalação de elevadores, reforam de elevadores, modernização de elevadores, embelezamento de elevadores, são paulo,">
        <META NAME="organization name" CONTENT="Ascensor Elevadores. São Paulo, SP">
        <meta name="language" content="PT-BR">
        <META NAME="revisit-after" CONTENT="3 Days">
        <meta name="robots" content="FOLLOW,INDEX,ALL">
        <link rel="stylesheet" href='<?= base_url() ?>css/reset.css' type="text/css" />
        <link rel="stylesheet" href='<?= base_url() ?>css/fontface.css' type="text/css" />
        <link rel="stylesheet" href='<?= base_url() ?>css/base_clientes.css' type="text/css" />
        <link rel="stylesheet" href='<?= base_url() ?>css/<?= $this->router->class ?>.css' type="text/css" />

        <script type='text/javascript' src='<?= base_url() ?>js/jquery-1.6.4.min.js'></script>
        <script type='text/javascript' src='<?= base_url() ?>js/jquery.corner.js'></script>
        <script type='text/javascript' src='<?= base_url() ?>js/cliente.js'></script>

    </head>
    <body>

        <div class="bg-wrapper">

        <div class="principal">

            <div class="faixa-laranja"></div>

            <span class="telefone">
                <img src="<?= base_url() ?>_imgs/layout/telefones.jpg">
            </span>

            <a href="<?= base_url() ?>index.php/cliente/home/logout">
                <img class="logotipo" src="<?= base_url() ?>_imgs/layout/logotipo.jpg">
            </a>

            <div class="flash-object">
                <a class="logout-btn" href="<?= base_url() ?>index.php/cliente/home/logout">SAIR [ X ]</a>

                <div class="info-logado">
                    <span class="info-logado-status">CLIENTE LOGADO</span>
                    <br/>
                    <?= $nome_cliente ?>
                    <br/>
                    <span class="info-logado-data">data : <?=date('d/m/Y')?></span>
                </div>
            </div>

            <div class="menu">

                <ul class="lista-menu">
                    <li class="primeiro">
                        <a href="<?= base_url('index.php') ?>/cliente/visitas">
                            A&nbsp;C&nbsp;O&nbsp;M&nbsp;P&nbsp;A&nbsp;N&nbsp;H&nbsp;A&nbsp;M&nbsp;E&nbsp;N&nbsp;T&nbsp;O&nbsp;&nbsp;D&nbsp;E&nbsp;&nbsp;V&nbsp;I&nbsp;S&nbsp;I&nbsp;T&nbsp;A&nbsp;S&nbsp;&nbsp;T&nbsp;É&nbsp;C&nbsp;N&nbsp;I&nbsp;C&nbsp;A&nbsp;S
                        </a>
                    </li>
                    <li>
                        <a href="<?= base_url('index.php') ?>/cliente/financeiro">
                            A&nbsp;C&nbsp;O&nbsp;M&nbsp;P&nbsp;A&nbsp;N&nbsp;H&nbsp;A&nbsp;M&nbsp;E&nbsp;N&nbsp;T&nbsp;O&nbsp;&nbsp;F&nbsp;I&nbsp;N&nbsp;A&nbsp;N&nbsp;C&nbsp;E&nbsp;I&nbsp;R&nbsp;O
                        </a>
                    </li>
                    
                </ul>

            </div>

            <br clear="all">

            <div class="centro">

                <br clear="all">