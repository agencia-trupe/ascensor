<!DOCTYPE html>
<html>
    <head>
        <title>Ascensor Elevadores · Instalação e manutenção de elevadores, São Paulo - SP</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="TITLE" content="Manutenção de elevadores. Instalação de elevadores. Reforma de elevadores. Modernização de Elevadores. Embelezamento de elevadores. São Paulo - SP">
        <meta name="SUBJECT" content="Manutenção, instalação, reforma, modernização e embelezamento de elevadores. São Paulo - SP.">
        <meta name="Description" content="Manutenção de elevadores. Instalação de elevadores. Reforma de elevadores. Modernização de Elevadores. Embelezamento de elevadores. São Paulo - SP">
        <meta name="keywords" content="manutenção de elevadores, instalação de elevadores, reforam de elevadores, modernização de elevadores, embelezamento de elevadores, são paulo,">
        <meta NAME="organization name" CONTENT="Ascensor Elevadores. São Paulo, SP">
        <meta name="language" content="PT-BR">
        <meta NAME="revisit-after" CONTENT="3 Days">
        <meta name="robots" content="FOLLOW,INDEX,ALL">
        <meta name="author" content="Trupe Agência Criativa">
        

        <base href="<?= base_url() ?>">
        <script> var BASE = '<?= base_url() ?>'</script>

        <?CSS(array('reset', 'base', 'fontface', 'fancybox/fancybox',$this->router->class))?>  
          

        <?if(ENVIRONMENT == 'development'):?>
            
            <?JS(array('modernizr-2.0.6.min', 'less-1.3.0.min', 'jquery-1.8.0.min', $this->router->class))?>
            
        <?else:?>

            <?JS(array('modernizr-2.0.6.min', $this->router->class, $load_js))?>
            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
            <script>window.jQuery || document.write('<script src="js/jquery-1.8.0.min.js"><\/script>')</script>

        <?endif;?>

    </head>
    <body>

        <div class="bg-wrapper">

        <div class="principal">

            <div class="faixa-laranja"></div>

            <span class="telefone">
                <img src="<?= base_url() ?>_imgs/layout/telefones.jpg">
            </span>

            <a href="index.php/home" title="Página Inicial" title="">
                <img class="logotipo" src="<?= base_url() ?>_imgs/layout/logotipo.jpg">
            </a>

            <div class="flash-object">

                <?php foreach ($topo as $key => $value): ?>
                    <div class="animate" <?if($key > 0)echo" style='display:none;'"?>>
                        <img src="_imgs/topo/<?=$value['imagem']?>">
                        <hgroup>
                            <h1><?=$value['titulo']?></h1>
                            <h2><?=$value['texto']?></h2>
                        </hgroup>
                    </div>
                <?php endforeach ?>

            </div>

            <div class="menu">

                <ul class="lista-menu">
                    <li class="primeiro">
                        <a href="index.php/quemsomos" title="Quem Somos" <?if($this->router->class=='quemsomos')echo" class='ativo'"?>>
                            Q&nbsp;U&nbsp;E&nbsp;M&nbsp;&nbsp;S&nbsp;O&nbsp;M&nbsp;O&nbsp;S
                        </a>
                    </li>
                    <li>
                        <a href="index.php/servicos" title="Serviços" <?if($this->router->class=='servicos')echo" class='ativo'"?>>
                            S&nbsp;E&nbsp;R&nbsp;V&nbsp;I&nbsp;Ç&nbsp;O&nbsp;S
                        </a>
                    </li>
                    <li>
                        <a href="index.php/produtos" title="Produtos" <?if($this->router->class=='produtos')echo" class='ativo'"?>>
                            P&nbsp;R&nbsp;O&nbsp;D&nbsp;U&nbsp;T&nbsp;O&nbsp;S
                        </a>
                    </li>
                    <li>
                        <a href="index.php/acessibilidade" title="Acessibilidade" <?if($this->router->class=='acessibilidade')echo" class='ativo'"?>>
                            A&nbsp;C&nbsp;E&nbsp;S&nbsp;S&nbsp;I&nbsp;B&nbsp;I&nbsp;L&nbsp;I&nbsp;D&nbsp;A&nbsp;D&nbsp;E
                        </a>
                    </li>
                    <li>
                        <a href="index.php/informacoes" title="Informações Úteis" <?if($this->router->class=='informacoes')echo" class='ativo'"?>>
                            I&nbsp;N&nbsp;F&nbsp;O&nbsp;R&nbsp;M&nbsp;A&nbsp;Ç&nbsp;Õ&nbsp;E&nbsp;S&nbsp;&nbsp;Ú&nbsp;T&nbsp;E&nbsp;I&nbsp;S
                        </a>
                    </li>
                    <li>
                        <a href="index.php/contato" title="Contato" <?if($this->router->class=='contato')echo" class='ativo'"?>>
                            C&nbsp;O&nbsp;N&nbsp;T&nbsp;A&nbsp;T&nbsp;O
                        </a>
                    </li>
                </ul>

                <? if($this->router->class == 'home'): ?>

                <div class="bg-wrapper-menu-home">

                <div class="login-clientes">

                    A&nbsp;C&nbsp;E&nbsp;S&nbsp;S&nbsp;O&nbsp;&nbsp;&nbsp;&nbsp;P&nbsp;A&nbsp;R&nbsp;A&nbsp;&nbsp;&nbsp;&nbsp;
                    C&nbsp;L&nbsp;I&nbsp;E&nbsp;N&nbsp;T&nbsp;E&nbsp;S<br/>

                    <form name="form-clientes" method="post" action="index.php/cliente">

                        <input type="text" name="clientes-login" value="LOGIN" onfocus="if(this.value=='LOGIN')this.value=''"
                               onblur="if(this.value=='')this.value='LOGIN'">
                        <input type="text" id="login-senha" name="clientes-senha" value="SENHA"
                               onfocus="this.type='password';if(this.value=='SENHA')this.value=''"
                               onblur="if(this.value==''){this.value='SENHA';this.type='text';}">
                        <input type="submit" value="entrar">

                    </form>

                </div>

                </div>

                <? endif; ?>

            </div>

            <div class="centro">