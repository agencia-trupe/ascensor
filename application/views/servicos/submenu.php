<div class="barra-lateral">

    <h2>SERVIÇOS</h2>
    <ul class="submenu">
        <li><a href="index.php/servicos/manutencao" <?if($this->router->method == 'manutencao')echo" class='ativo'"?>title="Manutenção 24 Horas"><span class="vermelho">&raquo;</span>Manutenção 24 Horas</a></li>

        <li><a href="index.php/servicos/modernizacao_tecnologica" <?if($this->router->method == 'modernizacao_tecnologica')echo" class='ativo'"?>title="Modernização Tecnológica"><span class="vermelho">&raquo;</span>Modernização Tecnológica</a></li>

        <li><a href="index.php/servicos/modernizacao_estetica" <?if($this->router->method == 'modernizacao_estetica')echo" class='ativo'"?>title="Modernização Estética"><span class="vermelho">&raquo;</span>Modernização Estética</a></li>

        <li><a href="index.php/servicos/laudos_e_vistorias" <?if($this->router->method == 'laudos_e_vistorias')echo" class='ativo'"?>title="Laudos e Vistorias"><span class="vermelho">&raquo;</span>Laudos e Vistorias</a></li>

        <li><a href="index.php/servicos/normas" <?if($this->router->method == 'normas')echo" class='ativo'"?>title="Adequação às normas"><span class="vermelho">&raquo;</span>Adequação às normas</a></li>

        <li><a href="index.php/servicos/acessibilidade" <?if($this->router->method == 'acessibilidade')echo" class='ativo'"?>title="Acessibilidade"><span class="vermelho">&raquo;</span>Acessibilidade <span class="menor">(instalação de plataformas)</span></a></li>

        <li><a href="index.php/servicos/antigos" <?if($this->router->method == 'antigos')echo" class='ativo'"?>title="Manutenção de elevadores antigos"><span class="vermelho">&raquo;</span>Manutenção de elevadores antigos</a></li>
    </ul>

</div>