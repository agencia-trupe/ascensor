<div class="conteudo">

    <h1 class="titulo-internas">Modernização Tecnológica</h1>
    
    <div class="box-laranja-internas">
        <img src="_imgs/layout/servicos-modern-tecnologica.jpg">
        <div class="texto">
            <h2>
                Tecnologia para transformar elevadores antigos em equipamentos seguros e modernos.
            </h2>
            <p>
                Quadro de Comando Computadorizado é um novo conceito para elevadores, sua utilização torna as viagens mais suaves, seguras e confortáveis, além de gerar grande economia de energia e diminuir a frequência na troca de peças.
            </p>
        </div>
    </div>

    <div class="texto-internas">

        <p>
            <strong>Modernização Tecnológica</strong> é o processo de substituição dos quadros de comando antigos por modelos computadorizados e a atualização dos demais componentes: transformadores, fontes, disjuntores termomagnéticos, contatores compactos e blindados (com seus respectivos componentes eletromecânicos e eletrônicos para acionamento da máquina de tração), freios, portas, limites e circuitos de segurança. 
        </p>

        <h3>
        	Por que modernizar?
        </h3>
        <p>
            <strong>Economia de energia</strong><br>
            Com o sistema VVVF o elevador parte com uma corrente bem menor, possibilitando a economia de cerca de 40% do consumo de energia elétrica.
        </p>

        <p>
            <i>Nota: O consumo de energia elétrica gasta pelos elevadores representa em média, cerca de 6% do gasto com energia.</i>
        </p>

        <p>
            <strong>Redução de custo de manutenção</strong><br>
            Além da redução do consumo de energia, o custo de manutenção também é reduzido com aumento da vida útil dos elementos mecânicos, especialmente discos e lonas de freio, cabos de tração, polias e redutor de tração que são muito menos solicitados pelo fato do elevador partir e parar suavemente.    
        </p>

        <p>
            <strong>Viagens mais suaves</strong><br>
            Com a velocidade controlada o usuário não sente o desconforto do arranque ou da frenagem. A suavidade da partida sem “tranco”, da parada e a redução do ruído, são claramente percebidas pelo passageiro.
        </p>

        <p>
            <strong>Nivelamento preciso</strong><br>
            O VVVF tem total domínio sobre o motor e não permite a formação de degrau na parada. O elevador apresenta o mesmo desempenho, independente da lotação da cabina, distância percorrida ou velocidade de cada viagem.
        </p>

        <h3>
            Segurança do equipamento
        </h3>

        <p>DUPLA SEGURANÇA garantida por um circuito eletrônico e outro eletromecânico que atuam simultaneamente no controle de portas, trincos, e limites.</p>
        <p>PROTEÇÃO DE PORTA DE MOTOR DE CABINA: dispositivo eletrônico que ajuda a evitar a queima do motor do operador de porta cabina no caso de falha dos limites.</p>
        <p>DUPLA PROTEÇÃO DO MOTOR DE TRAÇÃO: evita a queima do motor de tração através de proteção eletrônica (desligamento automático) e através de relê térmico.</p>
        <p>PROTEÇÃO F.I.F. que garante a total segurança em casos de falta ou inversão de fases de alimentação elétrica.</p>
    </div>

</div>

<br clear="all">