<div class="conteudo">

    <h1 class="titulo-internas">Modernização Estética</h1>
    
    <div class="box-laranja-internas">
        <img src="_imgs/layout/servicos-modern-estetica.jpg">
        <div class="texto">
            <h2>
                Embelezamento e valorização de elevadores
            </h2>
            <p>
                O melhor preço do mercado!
            </p>
        </div>
    </div>

    <div class="texto-internas">

        <p><p>
                Nosso departamento de embelezamento conta com profissionais especializados na execução de serviços em aço inox, fórmica, madeira e projetos especiais.
            </p>
            
            <p>
                Contamos com maquinários modernos, como guilhotinas e dobradeiras, para a fabricação própria de cabinas em aço escovado, e garantimos o melhor preço do mercado.
            </p>
            
            <p>
                O processo de embelezamento de cabinas de elevadores é composto por:
            </p>
            
            <p>
                1. Revestimento das paredes internas do elevador em:<br>
                - aço inox escovado<br>
                - aço inox polido<br>
                - fórmica<br>
                - madeira
            </p>
            
            <p>
                2. Substituição do sub-teto com aço inox e acrílico, com nova iluminação.
            </p>
            
            <p>
                3. Instalação de corrimão no painel traseiro do elevador, conforme as normas referentes a elevadores, garantindo maior segurança e conforto aos usuários e contribuindo para a preservação do espelho que é colocado logo acima deste.
            </p>
            
            <p>
                4. Substituição do piso da cabine por granito em diversas tonalidades. Já incluso rampa de acesso ao elevador em aço inox escovado, para melhor acessibilidade.
            </p>
            
            <p>
                A modernização estética proporciona beleza e modernidade a elevadores ultrapassados, valorizando muito os imóveis de seu condomínio.
            </p>

    </div>

</div>

<br clear="all">