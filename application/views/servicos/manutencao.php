<div class="conteudo">

    <h1 class="titulo-internas">Manutenção 24 Horas</h1>
    
    <div class="olho-laranja">
        Pronto atendimento 24 horas por dia,<br> todos os dias da semana,<br>inclusive sábados, domingos e feriados.
    </div>

    <div class="texto-internas">

        <p>
            Manutenção preventiva e corretiva, executada por uma equipe qualificada, com total conhecimento em elevadores de todas as marcas existentes no mercado.
        </p>

        <h3>
        	Frota própria identificada e equipe uniformizada
        </h3>
        <p>
            Frota própria composta por veículos novos, todos identificados com a marca ASCENSOR, e funcionários uniformizados para proporcionar segurança no atendimento.
        </p>

        <p>
            Para maior agilidade nossa frota também conta com motocicletas para os casos de urgência garantindo assim o menor tempo de parada dos elevadores e a rápida solução de imprevistos.
        </p>

        <h3>
            Metodologia    
        </h3>

        <p>
            Para todos os elevadores atendidos realizamos relatórios técnicos fotográficos e fornecemos informações para acompanhamento de todos os processos realizados.
        </p>

    </div>

</div>

<br clear="all">