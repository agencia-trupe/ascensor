<div class="barra-lateral">

    <h2>INFORMAÇÕES ÚTEIS</h2>
    <p>
        A <strong>Ascensor</strong> selecionou alguns textos e notícias úteis e importantes sobre o mercado de elevadores para ajudar você a se manter bem informado.
    </p>
</div>

<div class="conteudo">

    <h1 class="titulo-internas">Para saber mais sobre o mercado <br>de elevadores no Brasil</h1>
    
    <div class="texto-internas" style="margin-top:15px;">

    	<?php foreach ($lista as $key => $value): ?>
            
            <a href="index.php/informacoes/detalhes/<?=$value->slug?>" title="<?=$value->titulo?>" class="link-info">                
                <h2><?=$value->titulo?></h2>
                <?=$value->olho?>
                <div class="saiba-mais">saiba mais &raquo;</div>
            </a>

        <?php endforeach ?>

    </div>

</div>

<br clear="all">