<div class="barra-lateral">

    <h2>INFORMAÇÕES ÚTEIS</h2>
    <p>
        A <strong>Ascensor</strong> selecionou alguns textos e notícias úteis e importantes sobre o mercado de elevadores para ajudar você a se manter bem informado.
    </p>
</div>

<div class="conteudo">

    <h1 class="titulo-internas">Para saber mais sobre o mercado <br>de elevadores no Brasil</h1>
    
    <div class="texto-internas" style="margin-top:15px;">

        <div class="link-info nopad">                
            <h2><?=$detalhes->titulo?></h2>
            <br>
            <?=$detalhes->texto?>
        </div>

        <div class="voltar">
            <a href="index.php/informacoes" title="Voltar" class="btn-voltar">&laquo; voltar</a>
        </div>

    </div>

</div>

<br clear="all">