<div class="barra-lateral">
    <h2>REALIZANDO A MANUTENÇÃO PERIÓDICA DOS ELEVADORES  VOCÊ GARANTE:</h2>
    <ul>
        <li>&bull; Segurança</li>
        <li>&bull; Economia</li>
        <li>&bull; Valorização do imóvel</li>
        <li>&bull; Soluções rápidas</li>
        <li>&bull; Menos quebras de peças</li>
        <li>&bull; Investimento programado</li>
    </ul>
</div>

<div class="conteudo">

    <a href="index.php/acessibilidade" title="Plataformas elevatórias" class="box-laranja">
        <img src="_imgs/layout/home-plataformas.jpg">
        <div class="texto">
            <h1>Plataformas elevatórias</h1>
            <p>
                Promovendo acessibilidade em edificações residenciais 
                ou públicas, as plataformas Ascensor oferecem harmonia 
                no projeto arquitetônico, facilidade na instalação e menor 
                prazo de entrega.
            </p>
            <div class="saiba-mais">Saiba mais &raquo;</div>
        </div>
    </a>

    <a href="index.php/servicos/manutencao" title="Manutenção de Elevadores" class="chamada-home manutencao">
        <div class="chamada-titulo">MANUTENÇÃO DE ELEVADORES</div>
        <p>
            Pronto atendimento <br><strong>24 horas por dia</strong>, todos os dias da semana, inclusive sábados, domingos e feriados.
        </p>
        <div class="saiba-mais">saiba mais &raquo;</div>
    </a>

    <a href="index.php/servicos/modernizacao_estetica" title="Modernização Estética" class="chamada-home estetica">
        <div class="chamada-titulo">MODERNIZAÇÃO ESTÉTICA</div>
        <p>
            A modernização estética proporciona beleza aos elevadores e valorização dos imóveis de seu condomínio.
        </p>
        <div class="saiba-mais">saiba mais &raquo;</div>
    </a>

    <a href="index.php/servicos/modernizacao_tecnologica" title="Modernização Técnica" class="chamada-home tecnica">
        <div class="chamada-titulo">MODERNIZAÇÃO TECNOLÓGICA</div>
        <p>
            Tecnologia para transformar elevadores antigos em equipamentos mais seguros, modernos e econômicos.
        </p>
        <div class="saiba-mais">saiba mais &raquo;</div>
    </a>

</div>



<br clear="all">

<div class="certificacoes">
    <img src="<?= base_url() ?>_imgs/layout/home-crea.jpg" style="margin-right:25px;">
    <img src="<?= base_url() ?>_imgs/layout/home-contru.jpg" style="margin-right:60px;">
    
    <div class="cert-box prim">
        <h3>NOSSAS CERTIFICAÇÕES</h3>
        A ASCENSOR ELEVADORES está regularmente registrada nos órgãos competentes que fiscalizam
        empresas que atuam no ramo de Elevadores:<br/>
        <a href="http://www.prefeitura.sp.gov.br" title="Prefeitura da Cidade de São Paulo" target="_blank">Prefeitura da Cidade de São Paulo</a> | <a href="http://www.creasp.org.br/" title="CREA" target="_blank">CREA</a> |
        <a href="http://www.prefeitura.sp.gov.br/cidade/secretarias/controle_urbano/contru/index.php?p=4284" title="CONTRU" target="_blank">CONTRU</a>
    </div>

    <div class="cert-box seg">
        <h3>CUIDADOS</h3>
        Confira mais sobre cuidados e precauções na hora de contratar serviços para elevadores em nossa seção
        <a href="index.php/informacoes" title="Informações Úteis">INFORMAÇÕES ÚTEIS</a>
    </div>
</div>

<br clear="all">

<div class="chamadas">

    <div class="chamada-box prim">
        <a href="index.php/informacoes" title="Informações Úteis">
            <img src="<?= base_url() ?>_imgs/layout/home-chamada4.jpg">
            <div class="titulo">INFRAESTRUTURA ADEQUADA GARANTE SEGURANÇA</div>
            Além das certificações e filiações aos órgãos competentes que atestam a seriedade de nosso trabalho, a ASCENSOR possui sede com infraestrutura dedicada à prestação dosmelhores serviços. <strong>Conheça aqui.</strong>
        </a>
    </div>

    <div class="chamada-box seg">
        <a href="index.php/informacoes">
            <img src="<?= base_url() ?>_imgs/layout/home-chamada5.jpg">
            <div class="titulo">LEGISLAÇÃO & CIA.</div>
            Com a experiência adquirida em 20 anos de atuação a ASCENSOR ELEVADORES reuniu um vasto material sobre legislação e outros assuntos de interesse dos usuários e contratantes de serviços para elevadores. Confira mais em nossa<br> seção: <strong>INFORMAÇÕES ÚTEIS</strong>
        </a>
    </div>

</div>