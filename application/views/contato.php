<div class="barra-lateral">

    <h2>CONTATO</h2>
    
    <p>
        FALE CONOSCO<br>
        <a href="mailto:comercial@ascensor.com.br" title="Entre em contato">comercial@ascensor.com.br</a>
    </p>

    <?if($envio):?>

        <?if($envio_status):?>
            <div class="msg sucesso">Mensagem enviada com sucesso!<br/>Entraremos em contato assim que possível.</div>
        <?else:?>
            <div class="msg falha">Não foi possível enviar sua mensagem.<br/>Tente novamente em breve.</div>
        <?endif;?>

    <?else:?>

    <form name="form-contato" id="form-contato" method="post" action="index.php/contato/envio">

        <input type="text" name="nome" id="input-nome" placeholder="Nome" required>

        <input type="email" name="email" id="input-email" placeholder="E-mail" required>

        <input type="text" name="telefone" id="input-telefone" placeholder="Telefone">

        <textarea name="mensagem" id="input-mensagem" placeholder="Mensagem" required></textarea>

        <input type="submit" value="ENVIAR &raquo;" />

    </form>

    <?endif;?>

</div>

<div class="conteudo contato">

    <h1 class="titulo-internas">Nossos Telefones</h1>
    
    <div class="telefone-cont">11 3104-0029</div>

    <div class="telefone-emergencia">Emergência: 11 2843-6005 <span class="menor">(24 horas)</span></div>

    <h1 class="titulo-internas">Nossa Localização</h1>

    <p>
        Rua do Bosque, 327 &bull; Barra Funda<br>
        01136-000 &bull; São Paulo, SP <br/>
    </p>

    <iframe width="623" height="300" style="border:1px #DADBD6 solid;" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=pt-BR&amp;geocode=&amp;q=R.+do+Bosque,+327+-+Santa+Cec%C3%ADlia,+S%C3%A3o+Paulo+-+S%C3%A3o+Paulo,+Rep%C3%BAblica+Federativa+do+Brasil&amp;aq=0&amp;oq=Rua+do+B&amp;sll=-22.546052,-48.635514&amp;sspn=6.947345,11.634521&amp;ie=UTF8&amp;hq=&amp;hnear=R.+do+Bosque,+327+-+Santa+Cec%C3%ADlia,+S%C3%A3o+Paulo,+01136-000,+Rep%C3%BAblica+Federativa+do+Brasil&amp;t=m&amp;z=14&amp;ll=-23.525968,-46.651927&amp;output=embed"></iframe>
</div>


<br clear="all">