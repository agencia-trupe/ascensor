<div class="conteudo">

    <h1 class="titulo-internas">Plataformas verticais para<br>desníveis de até 4 metros</h1>
    
    <div class="texto-internas">

        <img src="_imgs/layout/acessbilidade-img2.jpg" style="margin:20px 0;" alt="Plataformas para mobilidade reduzida">

        <p>
            As Plataformas Verticais Ascensor foram projetadas para o auxílio de pessoas com mobilidade reduzida. Com dois modelos disponíveis para desníveis de até 4m, Cabinada e Meia cabina, permitem o fácil acesso entre andares com segurança e conforto.
        </p>

        <p>
            Desenvolvidas principalmente  para bancos, condomínios, escolas, shoppings, clínicas, comércio e demais locais públicos, as plataformas pode ser instaladas em diversos ambientes, permitindo aos usuários total autonomia sobre o equipamento com segurança e conforto.
        </p>

        <h3>
            Principais Benefícios
        </h3>
        <ul style="margin-bottom:20px">
            <li>Alta Tecnologia </li>
            <li>Silencioso e preciso</li>
            <li>Sistema de resgate para queda de energia</li>
            <li>Perfeita ergonomia</li>
            <li>Prazo de entrega reduzido</li>
            <li>Baixo impacto de projeto</li>
            <li>Não demanda poço</li>
            <li>Atende a todas as normas exigidas por lei (NBR 15655-1)</li>
            <li>Baixo consumo de energia</li>
        </ul>

        <h3>
            Especificações
        </h3>
        <ul style="margin-bottom:20px">
            <li>Modelo Cabina inteira (AC08) com paredes de 2m e teto com iluminação</li>
            <li>Modelo Meia Cabina (AC11) com paredes até 1,10m de altura – efeito panorâmico</li>
            <li>Cabinas em aço pintado na cor branco gelo</li>
            <li>Piso antiderrapante</li>
            <li>Sistema travamento das portas durante o movimento da plataforma</li>
            <li>Abertura das portas somente com a plataforma nivelada ao piso</li>
            <li>Válvula de segurança contra rompimento de tubulação</li>
            <li>Freio de segurança</li>
            <li>Botão de emergência para parada imediata, em qualquer posição</li>
        </ul>

        <h3>
            Funcionamento
        </h3>
        <ul style="margin-bottom:20px">
            <li>Acionamento: Hidráulico</li>
            <li>Percurso: Até 4 metros</li>
            <li>Velocidade: 6m/min</li>
            <li>Tensão: 220 V trifásico</li>
            <li>Potência: 2,2 kW</li>
        </ul>

        <h3>
            Capacidade/tamanhos de base
        </h3>
        <ul style="margin-bottom:20px">
            <li>Capacidade: 325kg</li>
            <li>Tamanhos de base: 0,90x1,40m / 1,10 x 1,40m</li>
        </ul>

    </div>

</div>

<br clear="all">