<div class="conteudo">

    <h1 class="titulo-internas">Plataformas verticais para<br>desníveis de até 2 metros</h1>
    
    <div class="texto-internas">

        <img src="_imgs/layout/acessbilidade-img1.jpg" style="margin:20px 0;" alt="Plataformas para mobilidade reduzida">

        <p>
        	As Plataformas Verticais Ascensor com alcance de até 2 metros foram projetadas para o auxílio de pessoas com mobilidade reduzida.<br>
            Desenvolvidas principalmente  para utilização em  bancos, condomínios, escolas, shoppings, clínicas, comércio e demais locais públicos, a plataforma pode ser instalada em ambientes internos e externos, permitindo aos usuários total autonomia sobre o equipamento com segurança e conforto.
        </p>

        <h3>
            Principais Benefícios
        </h3>
    	<ul style="margin-bottom:20px">
            <li>Alta tecnologia </li>
            <li>Perfeita ergonomia</li>
            <li>Prazo de entrega reduzido</li>
            <li>Baixo impacto de projeto</li>
            <li>Ocupa pouco espaço</li>
            <li>Não demanda poço</li>
            <li>Atende a todas as normas exigidas por lei (NBR 15655-1)</li>
            <li>Baixo consumo de energia</li>
            <li>Uso externo (resistente a interpéries)</li>
		</ul>

        <h3>
            Características Gerais
        </h3>
        <ul style="margin-bottom:20px">
            <li>Cabina em aço pintado na cor branco gelo</li>
            <li>Piso antiderrapante</li>
            <li>Sistema travamento das portas durante o movimento da plataforma</li>
            <li>Abertura das portas somente com a plataforma nivelada ao piso</li>
            <li>Sensores situados na parte inferior da plataforma detectam qualquer objeto abaixo da mesma, travando o seu movimento ao menor contato</li>
            <li>Botão de emergência para parada imediata, em qualquer posição</li>
            <li>Sensor de cancela - Bloqueia o movimento da plataforma caso a cancela seja levantada</li>
        </ul>

        <h3>
            Funcionamento
        </h3>
        <ul style="margin-bottom:20px">
            <li>Movimentação por Fuso Elétrico</li>
            <li>Acionamento por botão de pressão constante ou joystick</li>
            <li>Percurso: até 2 metros</li>
            <li>Velocidade: 2,5m/min</li>
            <li>Tensão (monofásica): 220V</li>
            <li>Potência: 750W</li>
        </ul>

        <h3>
            Capacidade/tamanhos de base
        </h3>
        <ul style="margin-bottom:20px">
            <li>Capacidade: 325kg</li>
            <li>Tamanhos de base: 0,90x1,40m  / 1,10 x 1,40m</li>
        </ul>

    </div>

</div>

<br clear="all">