<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Simplelogin Class
 *
 * Makes authentication simple
 *
 * Simplelogin is released to the public domain
 * (use it however you want to)
 * 
 * Simplelogin expects this database setup
 * (if you are not using this setup you may
 * need to do some tweaking)
 * 

	#This is for a MySQL table
	CREATE TABLE `users` (
	`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
	`username` VARCHAR( 64 ) NOT NULL ,
	`password` VARCHAR( 64 ) NOT NULL ,
	UNIQUE (
	`username`
	)
	);

 * 
 */
class Simplelogin_cliente
{
	var $CI;
	var $user_table = 'clientes';
        var $username_field = 'username';
        var $password_field = 'password';

	function Simplelogin()
	{
		// get_instance does not work well in PHP 4
		// you end up with two instances
		// of the CI object and missing data
		// when you call get_instance in the constructor
		//$this->CI =& get_instance();
	}

	/**
	 * Login and sets session variables
	 *
	 * @access	public
	 * @param	string
	 * @param	string
	 * @return	bool
	 */
	function login($user = '', $password = '') {

		//Put here for PHP 4 users
		$this->CI =& get_instance();

		//Make sure login info was sent
		if($user == '' OR $password == ''){
            return false;
            //die('Login ou Senha vazios');
        }
		

		//Check if already logged in
		if($this->CI->session->userdata($this->username_field) == $user){
            return true;
        }
		
		
		//Check against user table
		$query = $this->CI->db->get_where($this->user_table, array($this->username_field => $user));
		
		if ($query->num_rows() > 0) {
			$row = $query->row_array(); 
			
			//Check against password
			if(md5($password) != $row[$this->password_field]) {
				return false;
				//die('Senha Incorreta');
			}

		
			//Destroy old session
			$this->CI->session->sess_destroy();
			
			//Create a fresh, brand new session
			$this->CI->session->sess_create();
			
			//Remove the password field
			unset($row[$this->password_field]);
			
			//Set session data
			$this->CI->session->set_userdata($row);
			
			//Set logged_in to true
			$this->CI->session->set_userdata(array('logged_in_cliente' => true));
			
			//Login was successful			
			return true;
		} else {
			return false;
			//die('Usuario nao encontrado');
		}	

	}

	/**
	 * Logout user
	 *
	 * @access	public
	 * @return	void
	 */
	function logout() {
		//Put here for PHP 4 users
		$this->CI =& get_instance();

		//Destroy session
		$this->CI->session->sess_destroy();
	}
}
?>