<?php
class Usuarios_model extends CI_Model {

    var $username;
    var $password;
    var $email;

    function __construct()     {
        parent::__construct();
    }

    function dadosUsuarios($id = null){
        if($id)
            $query = $this->db->get_where('usuarios', array('id' => $id));
        else
            $query = $this->db->get('usuarios');
        return $query->result();
    }

    function inserir(){
        $this->email = $this->input->post('email');
        $this->username = $this->input->post('username');
        $this->password = md5($this->input->post('senha'));
        $confirmacao = md5($this->input->post('senha_conf'));
        if($confirmacao == $this->password)
            $this->db->insert('usuarios', $this);
    }

    function editar($id){
        $this->email = $this->input->post('email');
        $this->username = $this->input->post('username');
        $this->password = md5($this->input->post('senha'));
        $confirmacao = md5($this->input->post('senha_conf'));
        if($confirmacao == $this->password)
            $this->db->update('usuarios', $this, array('id' => $id));
    }

    function excluir($id){
        $this->db->delete('usuarios', array('id' => $id));
    }

}
?>
