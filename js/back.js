$(document).ready( function(){

    $('.btn-voltar').click( function(){
        window.history.back();
    });

    $('table tr:gt(1)').hover( function(){
        $(this).css('backgroundColor', '#CECECE');
    }, function(){
        $(this).css('backgroundColor', '#FFF');
    });

    $('.submenu').hide();

    $('.comsubmenu').click( function(event){
       event.preventDefault();
    });

    $('.comsubmenu').hover( function(){
        $('.submenu').not($(this).next('.submenu')).stop(true,true).slideUp('fast');
        $(this).next('.submenu').slideDown('normal');
        return false;
    });

    $('.submenu').hover( function(){
        return false
    }, function(){
        $(this).stop(true,true).slideUp('normal');
        return false;
    });

    $('#noticias-form').submit( function(){
        $('#tinymce').val(tinyMCE.get('tinymce').getContent());
        obrigatorios = {
            'titulo' : 'Título da Notícia',
            'data' : 'Data da publicação',
            'tinymce' : 'Texto da Notícia'
        };        
        return verifica(obrigatorios);
    });

    $('#clientes-form').submit( function(){
        if($('#edicao').length){
            obrigatorios = {
                'nome' : 'Identificação do Cliente',
                'username' : 'Login do Cliente'
            };
        }else{
            obrigatorios = {
                'nome' : 'Identificação do Cliente',
                'username' : 'Login do cliente',
                'password' : 'Senha do Cliente'
            };
        }
        if(verifica(obrigatorios)){
            if($('#password').val() != ''){
                if($('#password').val() != $('#senha_conf').val() ){
                    alert('As senhas informadas não conferem.')
                    return false;
                }else{
                    return true;
                }
            }else{
                return true;
            }
        }else{
            return false;
        }
    });

    $('#tecnicos-form').submit( function(){
        obrigatorios = {
          'nome' : 'Nome do Técnico'
        };
        return verifica(obrigatorios);
    });

    $('#usuarios-form').submit( function(){
        if($('#edicao').length){
            if($('#senha').val() != ''){
                obrigatorios = {
                    'nome' : 'Nome de Usuário',
                    'senha' : 'Senha',
                    'senha_conf' : 'Confirmação de Senha'
                };
            }else{
                obrigatorios = {
                    'nome' : 'Nome de Usuário'
                };
            }
            if(verifica(obrigatorios)){
                if($('#senha').val() != ''){
                    if($('#senha').val() != $('#senha_conf').val() ){
                        alert('As senhas informadas não conferem.')
                        return false;
                    }else{
                        return true;
                    }
                }else{
                    return true;
                }
            }else{
                return false;
            }
        }else{
            obrigatorios = {
                'nome' : 'Nome de Usuário',
                'senha' : 'Senha',
                'senha_conf' : 'Confirmação de Senha'
            };
            if(verifica(obrigatorios)){
                if($('#senha').val() != $('#senha_conf').val() ){
                    alert('As senhas informadas não conferem.')
                    return false;
                }else{
                    return true;
                }
            }else{
                return false;
            }
        }

    });

// CRIANDO VERIFICAÇÃO DE CAMPOS OBRIGATÓRIOS PARA AS SEÇÕES
// -Visita técnica
// -Acompanhamento Financeiro
//
//    $('#visitas-form').submit( function(){
//        obrigatorios = {
//            '' : ''
//        };
//        check_obrig = verifica(obrigatorios);
//    });

    function verifica(obrigatorios){
        retorno = true;
        for(var i in obrigatorios){
            if($('#'+i).val() == ''){
                alert(obrigatorios[i] + ' é obrigatório.');
                $('#'+i).focus();
                retorno = false;
                break;
            }
        }
        return retorno;
    }

    tinyMCE.init({
        language : "pt",
        mode : "textareas",
        theme : "advanced",
        theme_advanced_buttons1 : "bold,italic,underline,link,unlink",
        theme_advanced_buttons2 : "",
        theme_advanced_buttons3 : "",
        theme_advanced_toolbar_location : "top",
        plugins: "paste"
    });

    $('.datepicker').datepicker();

    $('.timepicker').timepicker({
        timeOnlyTitle: 'Selecione o horário',
	timeText: '',
	hourText: 'Hora',
	minuteText: 'Minutos',
	secondText: '',
	currentText: 'Agora',
	closeText: 'Ok',
        stepHour: 1,
	stepMinute: 5,
        showCurrent: false
    });

    $('#select-cliente-visita').live('change', function(){
        if($(this).val() != ''){
            $('.target').fadeOut('slow', function(){
                $.post(CI_ROOT+'index.php/painel/visitas/geraTabela', {'cliente' : $('#select-cliente-visita').val()}, function(retorno){
                    $('.target').html(retorno)
                    $('.target').fadeIn('slow');
                });
            });
        }
    });

    $('#select-cliente-financeiro').live('change', function(){
        if($(this).val() != ''){
            $('.target').fadeOut('slow', function(){
                $.post(CI_ROOT+'index.php/painel/financeiro_mensal/geraTabela', {'cliente' : $('#select-cliente-financeiro').val()}, function(retorno){
                    $('.target').html(retorno)
                    $('.target').fadeIn('slow');
                });
            });
        }
    });

    $('#select-cliente-pecas').live('change', function(){
        if($(this).val() != ''){
            $('.target').fadeOut('slow', function(){
                $.post(CI_ROOT+'index.php/painel/financeiro_pecas/geraTabela', {'cliente' : $('#select-cliente-pecas').val()}, function(retorno){
                    $('.target').html(retorno)
                    $('.target').fadeIn('slow');
                });
            });
        }
    });

    $('#select-cliente-servicos').live('change', function(){
        if($(this).val() != ''){
            $('.target').fadeOut('slow', function(){
                $.post(CI_ROOT+'index.php/painel/financeiro_servicos/geraTabela', {'cliente' : $('#select-cliente-servicos').val()}, function(retorno){
                    $('.target').html(retorno)
                    $('.target').fadeIn('slow');
                });
            });
        }
    });

    $('#select-cliente-total').live('change', function(){
        if($(this).val() != ''){
            if(!$('#target-mes').is(':visible')){
                $('#target-mes').slideDown('normal');
            }else{
                $('#select-mes-total').trigger('change');
            }
        }else{
            if($('#target-mes').is(':visible')){
                $('#target-mes').slideUp('normal');
            }
        }
    });

    $('#valor_pago').live('change', function(){
        if($(this).is(':checked')){
            $('.data_pagamento').slideDown('fast');
        }else{
            $('.data_pagamento').slideUp('fast');
            $('#data_pagamento').val('');
        }
    });

    $('#select-mes-total').live('change', function(){
        if($(this).val() != ''){
            $('.target').fadeOut('slow', function(){
                $.post(CI_ROOT+'index.php/painel/financeiro/geraTabela', {'cliente' : $('#select-cliente-total').val(), 'mes' : $('#select-mes-total').val()}, function(retorno){
                    $('.target').html(retorno)
                    $('.target').fadeIn('slow');
                });
            });
        }
    });

});

glow.ready(function(){
        new glow.widgets.Sortable(
                '#content .grid_5, #content .grid_6',
                {
                        draggableOptions : {
                                handle : 'h2'
                        }
                }
        );
});