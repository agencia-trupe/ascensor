CREATE DATABASE  IF NOT EXISTS `ascensor` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `ascensor`;
-- MySQL dump 10.13  Distrib 5.5.29, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: ascensor
-- ------------------------------------------------------
-- Server version	5.5.29-0ubuntu0.12.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tecnicos`
--

DROP TABLE IF EXISTS `tecnicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tecnicos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(140) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tecnicos`
--

LOCK TABLES `tecnicos` WRITE;
/*!40000 ALTER TABLE `tecnicos` DISABLE KEYS */;
INSERT INTO `tecnicos` VALUES (2,'Técnico teste');
/*!40000 ALTER TABLE `tecnicos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `valor_contratacao`
--

DROP TABLE IF EXISTS `valor_contratacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `valor_contratacao` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text,
  `valor` int(10) unsigned NOT NULL,
  `data_vencimento` date NOT NULL,
  `data_pagamento` date DEFAULT NULL,
  `cliente` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `valor_contratacao`
--

LOCK TABLES `valor_contratacao` WRITE;
/*!40000 ALTER TABLE `valor_contratacao` DISABLE KEYS */;
INSERT INTO `valor_contratacao` VALUES (1,'<p>Teste 1</p>',120000,'2011-09-27','2011-09-26',2),(2,'<p>Teste 212</p>',2532000,'2011-09-28','2011-09-30',2),(4,'<p>Teste 4</p>',15000,'2011-10-12',NULL,2),(6,'',2500000,'2011-09-07',NULL,1),(7,'<p>teste</p>',120000,'2011-10-13',NULL,2),(8,'<p>Manuten&ccedil;&atilde;o do m&ecirc;s de outubro/2011</p>',150000,'2011-10-31',NULL,3),(9,'<p>Manuten&ccedil;&atilde;o do m&ecirc;s de setembro/2011</p>',150000,'2011-09-30','2011-09-30',3),(10,'<p>Manuten&ccedil;&atilde;o de agosto/2011</p>',150000,'2011-08-31','2011-08-31',3),(11,'<p>Manuten&ccedil;&atilde;o do m&ecirc;s de novembro/2011</p>',150000,'2011-11-30',NULL,3);
/*!40000 ALTER TABLE `valor_contratacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `informacoes`
--

DROP TABLE IF EXISTS `informacoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `informacoes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(250) NOT NULL,
  `texto` text,
  `olho` text,
  `slug` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `informacoes`
--

LOCK TABLES `informacoes` WRITE;
/*!40000 ALTER TABLE `informacoes` DISABLE KEYS */;
INSERT INTO `informacoes` VALUES (5,'Algumas medidas de economia simples e eficazes','<p>A principal medida de economia de eletricidade relacionada aos elevadores consiste na troca do sistema de comando para um computadorizado.</p>\r\n<ul>\r\n<li>A economia pode chegar a 40% no consumo, e, quanto maior o n&uacute;mero de andares do condom&iacute;nio a economia tende ser maior</li>\r\n<li>O sistema de comando eletr&ocirc;nico, como &eacute; conhecido, al&eacute;m de economizar energia, evita desgastes em outras &aacute;reas. Por controlar a acelera&ccedil;&atilde;o e a desacelera&ccedil;&atilde;o, minimiza os desgastes nos sistemas de freio e nos cabos de a&ccedil;o.</li>\r\n<li>O comando eletr&ocirc;nico pode ser programado para controlar as chamadas, fazendo com que os andares sejam atendidos apenas pelo elevador mais pr&oacute;ximo.</li>\r\n<li>Elevadores que atendam a andares pares e &iacute;mpares tamb&eacute;m geram economia em pr&eacute;dios de maior porte.</li>\r\n</ul>\r\n<p><strong><br /></strong></p>\r\n<p><strong>Medidas para quem n&atilde;o tem o sistema eletr&ocirc;nico</strong></p>\r\n<ul>\r\n<li>Chamar apenas um elevador de cada vez</li>\r\n<li>Verificar o estado do quadro e das instala&ccedil;&otilde;es el&eacute;tricas relacionadas ao elevador.</li>\r\n<li>Na lavagem dos corredores, deve-se tomar cuidado para que a &aacute;gua n&atilde;o escorra pela porta dos elevadores. Isso pode danificar os cabos el&eacute;tricos do trinco, al&eacute;m de corroer as portas, principalmente as de madeira.</li>\r\n<li>Outra medida pode ser proteger as portas dos elevadores com panos, evitando que a &aacute;gua escorra para o fosso.</li>\r\n<li>A \"lavagem a seco\", com panos molhados, tamb&eacute;m evita o problema.</li>\r\n<li>Mudan&ccedil;as e reformas no edif&iacute;cio tamb&eacute;m podem danificar os elevadores. Por isso, os mesmos devem estar protegidos quando forem transportar esses materiais.</li>\r\n<li>Excesso de peso n&atilde;o acarreta um consumo maior de energia, mas causa desgaste do equipamento.</li>\r\n</ul>','<p>A principal medida de economia de eletricidade relacionada aos elevadores consiste na troca do sistema de comando para um computadorizado.</p>','algumas_medidas_de_economia_simples_e_eficazes'),(6,'Exigências e normas da Prefeitura de São Paulo para elevadores','<p>Em S&atilde;o Paulo, a Lei Municipal n&ordm; 10.348/87 determina que qualquer elevador, de pessoas ou de carga, necessita de licen&ccedil;a para ser instalado. Al&eacute;m da licen&ccedil;a para instala&ccedil;&atilde;o existem taxas a serem recolhidas durante o per&iacute;odo de utiliza&ccedil;&atilde;o e manuten&ccedil;&atilde;o, regulamenta&ccedil;&atilde;o das empresas que instalam e d&atilde;o manuten&ccedil;&atilde;o para elevadores&nbsp; e a obten&ccedil;&atilde;o de alvar&aacute; para o funcionamento. A obten&ccedil;&atilde;o do alvar&aacute; s&oacute; &eacute; concedida para instala&ccedil;&otilde;es que seguem as regras da ABNT e foram realizadas por empresas aptas a realizar os servi&ccedil;os de instala&ccedil;&atilde;o e manuten&ccedil;&atilde;o, como a Ascensor.</p>\r\n<p>&nbsp;</p>\r\n<p>Conhe&ccedil;a os principais documentos disponibilizados pela prefeitura a esse respeito:</p>\r\n<p>- Taxa de licen&ccedil;a de elevador</p>\r\n<p><a href=\"http://www.prefeitura.sp.gov.br/cidade/secretarias/controle_urbano/contru/elevadores/index.php?p=28799\">http://www.prefeitura.sp.gov.br/cidade/secretarias/controle_urbano/contru/elevadores/index.php?p=28799</a></p>\r\n<p>&nbsp;</p>\r\n<p>- Alvar&aacute; de instala&ccedil;&atilde;o de elevadores</p>\r\n<p><a href=\"http://www.prefeitura.sp.gov.br/cidade/secretarias/controle_urbano/contru/elevadores/index.php?p=28800\">http://www.prefeitura.sp.gov.br/cidade/secretarias/controle_urbano/contru/elevadores/index.php?p=28800</a></p>\r\n<p>&nbsp;</p>\r\n<p>- Alvar&aacute; de funcionamento de elevadores</p>\r\n<p><a href=\"http://www.prefeitura.sp.gov.br/cidade/secretarias/controle_urbano/contru/elevadores/index.php?p=28801\">http://www.prefeitura.sp.gov.br/cidade/secretarias/controle_urbano/contru/elevadores/index.php?p=28801</a></p>\r\n<p>&nbsp;</p>\r\n<p>- Alvar&aacute; de instala&ccedil;&atilde;o e funcionamento de elevadores</p>\r\n<p><a href=\"http://www.prefeitura.sp.gov.br/cidade/secretarias/controle_urbano/contru/elevadores/index.php?p=28802\">http://www.prefeitura.sp.gov.br/cidade/secretarias/controle_urbano/contru/elevadores/index.php?p=28802</a></p>\r\n<p>&nbsp;</p>\r\n<p>- Registro para conserva&ccedil;&atilde;o de elevadores</p>\r\n<p><a href=\"http://www.prefeitura.sp.gov.br/cidade/secretarias/controle_urbano/contru/elevadores/index.php?p=28665\">http://www.prefeitura.sp.gov.br/cidade/secretarias/controle_urbano/contru/elevadores/index.php?p=28665</a></p>\r\n<p>&nbsp;</p>\r\n<p>- Rela&ccedil;&atilde;o das empresas consservadoras</p>\r\n<p><a href=\"http://www.prefeitura.sp.gov.br/cidade/secretarias/upload/chamadas/novembro_2012_1352486348.pdf\">http://www.prefeitura.sp.gov.br/cidade/secretarias/upload/chamadas/novembro_2012_1352486348.pdf</a></p>\r\n<p>&nbsp;</p>\r\n<p>- Entrada e baixa de responsabilidade t&eacute;cnica pela empresa conservadora de aparelhos</p>\r\n<p><a href=\"http://www.prefeitura.sp.gov.br/cidade/secretarias/controle_urbano/contru/elevadores/index.php?p=28804\">http://www.prefeitura.sp.gov.br/cidade/secretarias/controle_urbano/contru/elevadores/index.php?p=28804</a></p>\r\n<p>&nbsp;</p>\r\n<p>- Procedimentos para substitui&ccedil;&atilde;o, reforma, moderniza&ccedil;&atilde;o e remodela&ccedil;&atilde;o est&eacute;tica de aparelhos de transportes</p>\r\n<p><a href=\"http://www.prefeitura.sp.gov.br/cidade/secretarias/controle_urbano/contru/elevadores/index.php?p=39808\">http://www.prefeitura.sp.gov.br/cidade/secretarias/controle_urbano/contru/elevadores/index.php?p=39808</a></p>\r\n<p>&nbsp;</p>\r\n<p>- Legisla&ccedil;&atilde;o RIA online</p>\r\n<p><a href=\"http://www.prefeitura.sp.gov.br/cidade/secretarias/controle_urbano/contru/elevadores/index.php?p=28805\">http://www.prefeitura.sp.gov.br/cidade/secretarias/controle_urbano/contru/elevadores/index.php?p=28805</a></p>','<p>As prefeituras implantam e exigem o cumprimento de diversas normas e taxas para a instala&ccedil;&atilde;o, opera&ccedil;&atilde;o e manuten&ccedil;&atilde;o de elevadores em cada munic&iacute;pio. Conhe&ccedil;a as regras da Prefeitura de S&atilde;o Paulo.</p>','exigncias_e_normas_da_prefeitura_de_so_paulo_para_elevadores'),(7,'NORMAS: Norma da ABNT visa mais segurança em elevadores','<p>Segundo a ABNT, a NBR 15.597 estabelece regras para melhoria da seguran&ccedil;a dos elevadores de passageiros existentes, com o objetivo de atingir um n&iacute;vel equivalente de seguran&ccedil;a &agrave;quele de um elevador recentemente instalado e aplicando o que h&aacute; de mais avan&ccedil;ado em seguran&ccedil;a. Isso quer dizer que os elevadores antigos dever&atilde;o se atualizar, alcan&ccedil;ando um padr&atilde;o de seguran&ccedil;a igual, ou pr&oacute;ximo, ao dos novos equipamentos.</p>\r\n<p><strong>Itens exigidos nos elevadores</strong></p>\r\n<ul>\r\n<li>Braile nos pavimentos e na cabina;</li>\r\n<li>Luz de emerg&ecirc;ncia;</li>\r\n<li>Interfone ligado &agrave; portaria e &agrave; casa de m&aacute;quinas;</li>\r\n<li>Aviso sonoro informando o andar;</li>\r\n<li>Nivelamento preciso da cabina nos andares;</li>\r\n<li>Caixa de inspe&ccedil;&atilde;o no topo da cabina; escada de acesso ao po&ccedil;o;</li>\r\n<li>Placas de advert&ecirc;ncia e sinaliza&ccedil;&otilde;es nas portas de pavimento, principalmente quando o elevador estiver em manuten&ccedil;&atilde;o e tamb&eacute;m aquelas de aten&ccedil;&atilde;o para o usu&aacute;rio verificar se o elevador encontra-se no andar antes de entrar;</li>\r\n<li>Guarda corpo (espa&ccedil;o cercado localizado no lado externo do teto do elevador para garantir a seguran&ccedil;a do t&eacute;cnico durante a manuten&ccedil;&atilde;o do equipamento);</li>\r\n<li>Protetores de polia de tra&ccedil;&atilde;o e do limitador de velocidade.</li>\r\n</ul>\r\n<p>NOTA: em alguns casos h&aacute; necessidade de substitui&ccedil;&atilde;o do limitador de velocidade por um atual para adequar &agrave;s normas vigentes quanto &agrave; trava de seguran&ccedil;a.</p>','<p>Para diminuir a incid&ecirc;ncia&nbsp; de acidentes, a ABNT (Associa&ccedil;&atilde;o Brasileira de Normas T&eacute;cnicas) validou, em 19 de setembro de 2008, a NBR 15.597. A norma trata dos requisitos de seguran&ccedil;a para a constru&ccedil;&atilde;o e instala&ccedil;&atilde;o de elevadores - elevadores existentes - requisitos para melhoria da seguran&ccedil;a dos elevadores el&eacute;tricos de passageiros e elevadores el&eacute;tricos de passageiros e cargas.</p>','normas_norma_da_abnt_visa_mais_segurana_em_elevadores'),(8,'Reforma de elevadores valoriza os apartamentos em 20% em média','<p>Ascensor, empresa de elevadores com sede em&nbsp;S&atilde;o Paulo desenvolveu cabines de elevadores que podem ser adequadas &agrave;s necessidades e &agrave; proposta visual do condom&iacute;nio. <br /> Outra inova&ccedil;&atilde;o que traz economia e valoriza os edif&iacute;cios s&atilde;o os elevadores que n&atilde;o utilizam a chamada casa de m&aacute;quinas. O empreendimento praticamente ganha mais um andar, que pode ser aproveitado de acordo com as reais necessidades dos moradores.</p>\r\n<p>Com a m&aacute;quina acoplada no pr&oacute;prio elevador, diminuem-se tamb&eacute;m ru&iacute;do e vibra&ccedil;&otilde;es tanto no edif&iacute;cio quanto dentro da cabine. Esse modelo foi usado nos Jogos Pan-Americanos, no Rio de Janeiro, para transportar os atletas.</p>\r\n<p>Considerado o meio de transporte mais seguro do mundo, o elevador evoluiu nos &uacute;ltimos anos, acompanhando os avan&ccedil;os tecnol&oacute;gicos em v&aacute;rios pa&iacute;ses. No entanto, ao contr&aacute;rio do que muitos possam imaginar, a substitui&ccedil;&atilde;o total de um elevador &eacute; rara. A moderniza&ccedil;&atilde;o do equipamento &eacute; feita de forma gradual, de acordo com os interesses e a disponibilidade financeira de cada edif&iacute;cio. <br /> Desde que a manuten&ccedil;&atilde;o preventiva e corretiva seja feita regularmente por empresas especializadas, e tamb&eacute;m registradas na prefeitura, a vida &uacute;til do elevador pode passar dos 40 anos. Nos novos empreendimentos, a tecnologia nos elevadores j&aacute; &eacute; um requisito b&aacute;sico. Sistemas inteligentes fazem com que o equipamento economize viagens e chegue mais r&aacute;pido ao seu destino, al&eacute;m de evitar trancos, degraus em rela&ccedil;&atilde;o ao pavimento e n&atilde;o deixar ningu&eacute;m mais preso por uma eventual queda de energia.</p>\r\n<p>Empresas nacionais desenvolvem solu&ccedil;&otilde;es com tecnologia de 100% brasileira, como o elevador com comando de voz, &uacute;til para pessoa com dificuldade de locomo&ccedil;&atilde;o ou quando o elevador est&aacute; lotado, j&aacute; que ningu&eacute;m precisa se desdobrar para apertar o bot&atilde;o.</p>\r\n<p>O comando de voz &eacute; apenas um recurso adicional, j&aacute; que o elevador tamb&eacute;m funciona com toque no painel. H&aacute; tamb&eacute;m um sistema de transponder, um aparelho do tamanho de um chaveiro. Quando o morador entra no elevador, seus dados s&atilde;o identificados e ele &eacute; levado automaticamente ao seu andar.<br /> Uma das novidades que mais t&ecirc;m feito sucesso &eacute; o acionamento do elevador pela identifica&ccedil;&atilde;o das digitais. Um dos principais benef&iacute;cios &eacute; a seguran&ccedil;a, j&aacute; que at&eacute; mesmo os dias e hor&aacute;rios dos empregados podem ser restringidos. Se h&aacute; um funcion&aacute;rio que trabalha toda quarta-feira, somente nesse dia o elevador aceitar&aacute; a identifica&ccedil;&atilde;o.</p>\r\n<p><em>Fonte: Jornal da Tarde</em></p>','<p>Uma reforma superficial e relativamente simples no elevador pode valorizar os apartamentos de um pr&eacute;dio at&eacute; 20%. Instalar espelhos, melhorar a ilumina&ccedil;&atilde;o e trocar o painel s&atilde;o medidas de excelente custo-benef&iacute;cio, e que d&atilde;o resultado imediato, afirmam executivos do setor.</p>','reforma_de_elevadores_valoriza_os_apartamentos_em_20_em_mdia');
/*!40000 ALTER TABLE `informacoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(140) NOT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  `telefone1` varchar(15) DEFAULT NULL,
  `telefone2` varchar(15) DEFAULT NULL,
  `username` varchar(140) NOT NULL,
  `password` varchar(140) NOT NULL,
  `email` varchar(140) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `LoginCliente` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (1,'Teste','','','','trupe','d32c9694c72f1799ec545c82c8309c02',NULL),(2,'Teste 2','','','','tetere','00a08728d08702eafc2c1f8e8622e40c',NULL),(3,'Cliente','','','','cliente','4983a0ab83ed86e0e7213c8783940193',NULL);
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `visitas`
--

DROP TABLE IF EXISTS `visitas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visitas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` datetime NOT NULL,
  `ocorrencia` text NOT NULL,
  `tecnico` int(10) unsigned NOT NULL,
  `cliente` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visitas`
--

LOCK TABLES `visitas` WRITE;
/*!40000 ALTER TABLE `visitas` DISABLE KEYS */;
INSERT INTO `visitas` VALUES (3,'2011-09-28 09:00:00','<p>Teste 31</p>',2,1),(4,'2011-09-29 05:00:00','',2,2),(5,'2011-09-27 08:00:00','',2,2),(7,'2011-09-28 09:00:00','',2,2),(8,'2011-09-27 11:00:00','',2,2),(9,'2011-09-27 17:00:00','',2,2),(11,'2011-09-29 06:00:00','',2,2),(12,'2011-09-07 04:00:00','',2,2),(13,'2011-10-12 09:00:00','<p>Visita t&eacute;cnica</p>',2,3),(14,'2011-09-15 15:00:00','<p>Visita T&eacute;cnica 123</p>',2,3),(15,'2011-08-17 06:40:00','<p>Ocorr&ecirc;ncia teste</p>',2,3),(16,'2011-10-03 08:00:00','<p>Troca do rolamento superior do elevador de servi&ccedil;o</p>',2,3);
/*!40000 ALTER TABLE `visitas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `password` varchar(140) NOT NULL,
  `email` varchar(140) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `NomeUsuario` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'trupe','d32c9694c72f1799ec545c82c8309c02','contato@trupe.net');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `valor_pecas`
--

DROP TABLE IF EXISTS `valor_pecas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `valor_pecas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `produto` varchar(140) NOT NULL,
  `descritivo` text,
  `quantidade` varchar(10) NOT NULL,
  `data` date NOT NULL,
  `valor` int(10) unsigned NOT NULL,
  `cliente` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `valor_pecas`
--

LOCK TABLES `valor_pecas` WRITE;
/*!40000 ALTER TABLE `valor_pecas` DISABLE KEYS */;
INSERT INTO `valor_pecas` VALUES (1,'Produto teste edit','<p>Editado 213</p>','58','2011-09-30',150000,2),(2,'Produto ABC','<p>Descritivo do servi&ccedil;o e terer&eacute;un terer&eacute;un</p>','1','2011-09-30',150000,2),(3,'Peça tal tal tal','<p>Pe&ccedil;a tal tal tal da parte superior bla bla</p>','2','2011-10-12',30000,3);
/*!40000 ALTER TABLE `valor_pecas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `valor_servicos`
--

DROP TABLE IF EXISTS `valor_servicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `valor_servicos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `servico` varchar(140) NOT NULL,
  `descritivo` text,
  `data` date NOT NULL,
  `valor` int(10) unsigned NOT NULL,
  `cliente` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `valor_servicos`
--

LOCK TABLES `valor_servicos` WRITE;
/*!40000 ALTER TABLE `valor_servicos` DISABLE KEYS */;
INSERT INTO `valor_servicos` VALUES (1,'Serviço Teste','<p>Descritivo do servi&ccedil;o blablabla</p>','2011-09-30',150000,2),(2,'Troca da peça tal tal tal','<p>Troca da pe&ccedil;a tal tal tal da parte superior</p>','2011-10-12',20000,3);
/*!40000 ALTER TABLE `valor_servicos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-02-20 14:01:57
